��          �       |      |  '   }     �     �     �     �     �  2   �          ,     3  
   6     A     X     ]     b     j  $   o  #   �  @   �  <   �     6  "  O  '   r     �     �     �     �     �  2   �          !     (  
   +     6     M     R     W     _  $   d  #   �  @   �  <   �     +   A valid Email address must be provided. Always Assign to group: Conditional Logic Double Opt-In Email Address Enter a feed name to uniquely identify this setup. Gravity Forms Groups If Map Fields Mark subscriber as VIP Name Note Options Tags Unable to add note to subscriber: %s Unable to add/update subscriber: %s Unable to check if email address is already used by a member: %s Unable to process feed because API could not be initialized. https://gravityforms.com PO-Revision-Date: 2022-06-27 08:43:13+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-rc.4
Language: en_AU
Project-Id-Version: RocketGenius - Gravity Forms Mailchimp Add-On
 A valid email address must be provided. Always Assign to group: Conditional Logic Double Opt-In Email Address Enter a feed name to uniquely identify this setup. Gravity Forms Groups If Map Fields Mark subscriber as VIP Name Note Options Tags Unable to add note to subscriber: %s Unable to add/update subscriber: %s Unable to check if email address is already used by a member: %s Unable to process feed because API could not be initialised. https://gravityforms.com 