<?php
/*
Plugin Name: Risk Assessment Quiz 2.0
Plugin URI:  https://balonka.se
Version:     1.1
Author:      Balonka AB
Author URI:  https://balonka.se
Text Domain: balonka-quiz
Domain Path: /languages
*/

namespace balonka\quiz;

class Plugin {
    const TEXT_DOMAIN = 'balonka-quiz';

    private static $instance;

	public $url;
    public $dir;
    private $options_ns = 'balonka_quiz_options';
    private $settings = [];
    private $template;
    private $forms = [];
    private $gateways = [];
    private $post_types = [];


    private function __construct() {ini_set('display_errors', true);
        $this->url = plugins_url('', __FILE__);
        $this->dir = __DIR__;
        $this->settings = get_option($this->options_ns, []);

        require_once __DIR__ . '/functions.php';
        require_once __DIR__ . '/includes/Loader.php';
        //require_once __DIR__ . '/lib/vendor/autoload.php';

        Loader::mapNamespace(__NAMESPACE__, __DIR__ . '/includes');

        Logger::set_defaults([
            'save_path' => __DIR__ . '/logs'
        ]);


        $this->register_types();
        new shortcodes\Quiz_Graph\Quiz_Graph;

        /* $missions_form = new forms\missions\Missions;
        $gift_form = new forms\gift\Gift;
        $recurring_form = new forms\recurring\Recurring;
        $this->forms[$missions_form->get_type()] = $missions_form;
        $this->forms[$gift_form->get_type()] = $gift_form;
        $this->forms[$recurring_form->get_type()] = $recurring_form; */

        add_action('admin_menu', [$this, 'register_admin_menu']);
        add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_scripts']);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
        add_action('wp_ajax_save_settings', [$this, 'ajax_save_settings']);
        add_action('wp_ajax_load_form_survey_fields_scores', [$this, 'ajax_load_form_survey_fields_scores']);
        add_action('admin_init', [$this, 'admin_init']);

        add_action('gform_after_submission', [$this, 'gform_after_submission'], 10, 2);
        add_filter('gform_confirmation', [$this, 'gform_confirmation'], 10, 4);
        add_filter('gform_custom_merge_tags', [$this, 'gform_custom_merge_tags'], 10, 4);
        add_filter('gform_replace_merge_tags', [$this, 'gform_replace_merge_tags'], 10, 7);
    }

    private function register_types() {
        $type = 'form-setup';
        $form_setup = new PostType($type, 'Form Setup');
        $form_setup->set_parent_menu('balonka-quiz');

        $form_setup->set_args([
            'public'             	=> false,
            'publicly_queryable' 	=> false,
            'show_ui'            	=> true,
            'show_in_menu'       	=> true,
            'exclude_from_search'   => true,
            'supports'              => ['title']
        ]);

        $form_setup->add_meta_box('form-setup', [
            'title' => 'Form Setup',
            'context' => 'normal',
            'priority' => 'high',
            'render' => [$this, 'render_form_setup_metabox'],
            'save' => function($post_id) {
                update_post_meta($post_id, 'form_id', $_POST['form_id']);
                update_post_meta($post_id, 'survey_scores', $_POST['survey_scores']);
                update_post_meta($post_id, 'survey_diagram_labels', $_POST['survey_diagram_labels']);
                update_post_meta($post_id, 'survey_results', $_POST['survey_results']);
                update_post_meta($post_id, 'survey_conditional_messages', $_POST['survey_conditional_messages']);
                update_post_meta($post_id, 'survey_result_page', $_POST['survey_result_page']);
                update_post_meta($post_id, 'survey_result_page_redirect', $_POST['survey_result_page_redirect']);
            }
        ]);

        $this->post_types[$type] = $form_setup;
    }

    public function get_post_type($type) {
        return isset($this->post_types[$type]) ? $this->post_types[$type] : null;
    }


    public static function getInstance() {
        if(empty(self::$instance)) {
            self::$instance = new static;
            do_action('balonka-quiz-constructed');
        }

        return self::$instance;
    }

    public function admin_init() {

    }


    public function admin_enqueue_scripts() {
        $screen = get_current_screen();
        $load_ids = ['form-setup'];

        foreach($load_ids as $id) {
            if(strstr($screen->id, $id)) {
                wp_enqueue_style('quiz-admin-style', $this->url . '/assets/quiz.admin.css');
                wp_enqueue_script('jquery.repeatable', $this->url . '/assets/jquery.repeatable.js', ['jquery'], false, true);
                wp_enqueue_script('quiz-admin-script', $this->url . '/assets/quiz.admin.js', ['jquery', 'jquery.repeatable'], false, true);
                wp_localize_script('quiz-admin-script', 'quiz_config', [
                    'ajax_url'      => admin_url( 'admin-ajax.php' ),
                    'save_settings' => 'save_settings'
                ]);

                /* foreach($this->gateways as $gateway) {
                    $gateway->admin_enqueue_scripts();
                } */

                break;
            }
        }
    }

    public function enqueue_scripts() {
        /* wp_enqueue_style('donation-style', $this->url . '/assets/donation.css');
        wp_enqueue_script('donation-script', $this->url . '/assets/donation.js', ['jquery'], false, true);
        wp_localize_script('donation-script', 'donation_config', [
            'ajax_url'      => admin_url( 'admin-ajax.php' ),
        ]); */

        wp_enqueue_style('quiz-css', $this->url . '/assets/quiz.css');
        wp_enqueue_script('chart.js', 'https://cdn.jsdelivr.net/npm/chart.js@3.6.0/dist/chart.min.js', [], '3.6.0', true);
        wp_enqueue_script('quiz-script', $this->url . '/assets/quiz.js', ['jquery', 'chart.js'], false, true);
    }

    public function register_admin_menu() {
        add_menu_page(
            __('Risk Assessment Quiz 2.0', self::TEXT_DOMAIN),
            __('Risk Assessment Quiz 2.0', self::TEXT_DOMAIN),
            'manage_options',
            'balonka-quiz',
            [$this, 'render_page']
        );

        /* add_submenu_page(
			'donation',
			__('Donation Settings', self::TEXT_DOMAIN),
			__('Settings', self::TEXT_DOMAIN),
			'manage_options',
			'donation/settings',
			[$this, 'render_page']
        ); */
    }

    public function render_page() {
        $textdomain = self::TEXT_DOMAIN;

        switch($_GET['page']) {
            default:
                $settings = $this->settings;

                $selected_tab = 'settings';
                ob_start();
                include 'templates/settings-fields.php';

                $fields = $this->apply_field_ns(
                    ob_get_clean(),
                    $this->options_ns
                );
        }


        include __DIR__ . '/templates/main.php';
    }

    public function render_form_setup_metabox($post) {
        $textdomain = self::TEXT_DOMAIN;
        $forms = get_gravity_forms();

        $additional_fields = $this->load_form_additional_fields($post->ID, '', $meta);

        include 'templates/form-setup-metabox.php';
    }

    private function apply_field_ns($input, $ns) {
		return preg_replace_callback(
			'/(<(?: input | select | textarea) \s+ [^>]* (?<=\s) name \s*=\s* ["\']) ( (?(?<=") [^"]+ | [^\']+) )/isx',
			function($matches) use($ns) {
				return $matches[1] . preg_replace(
					'/^[^\[\]]+/', $ns.'[$0]', $matches[2]
				);
			},
			$input
		);
    }


    public function ajax_save_settings() {
        $data = [];
        if(!empty($_POST['data'])) {
            parse_str($_POST['data'], $data);
        } else {
            $data = $_POST;
        }

        $status = ['success' => false];

        if(isset($data[$this->options_ns])) {
            $status['success'] = update_option($this->options_ns, $data[$this->options_ns], false);
        }

        echo json_encode($status);
        wp_die();
    }

    public function ajax_load_form_survey_fields_scores() {
        if(!empty($post_id = $_POST['post_id']) && !empty($form_id = $_POST['form_id'])) {
            echo $this->load_form_additional_fields($post_id, $form_id);

            wp_die();
        }
    }

    protected function load_form_additional_fields($post_id, $form_id = 0, &$meta = []) {
        $meta = []; $form_setup = new FormSetup($post_id);
        foreach([
            'form_id', 'survey_scores', 'survey_results',
            'survey_conditional_messages', 'survey_result_page',
            'survey_result_page_redirect', 'survey_diagram_labels'
        ] as $field) {
            if(!$form_id || $field == 'form_id' || $meta['form_id'] == $form_id) {
                $fn = 'get_meta_' . $field;
                $callback = [$form_setup, $fn];
                $meta[$field] = is_callable($callback) ? call_user_func($callback) : $form_setup->meta->$field;
            }
        }

        if($form_id && $form_id != $meta['form_id']) {
            $meta['form_id'] = $form_id;
        }

        $out = '';
        extract($meta);

        if($form_id) {
            $fields = get_gravity_form_fields($form_id, 'survey');
            if(!empty($fields)) {
                ob_start();
                include 'templates/form-setup-metabox-survey-fields-scores.php';
                $out = ob_get_clean();

                ob_start();
                include 'templates/form-setup-metabox-result-messages.php';
                $out .= ob_get_clean();
            }
        }

        return $out;
    }


    protected function add_filename_suffix($filename, $suffix) {
        return preg_replace_callback(
            '/^(.*)(\.[^.]+)$/i',
            function($matches) use($suffix) {
                return $matches[1] . $suffix . $matches[2];
            },
            $filename
        );
    }


    public function get_setting($name, $default = null) {
        return isset($this->settings[$name]) ? $this->settings[$name] : $default;
    }


    private function walk_dir($path, callable $callback) {
        if(is_dir($path)) {
            $dir = dir($path);

            while($entry = $dir->read()) {
                if($entry != '.' && $entry != '..') {
                    $entry_full = $path . '/' . $entry;
                    call_user_func($callback, $entry, $entry_full);
                }
            }

            $dir->close();
        }
    }

    public function get_form($type) {
        if(isset($this->forms[$type])) {
            return $this->forms[$type];
        }
    }

    private function load_payment_gateways() {
        $dir = $this->dir . '/includes/gateways';
        $this->walk_dir($dir, function($entry, $entry_full) {
            $parent_class = __NAMESPACE__.  '\PaymentGateway';
            $class = __NAMESPACE__ . '\gateways\\'. $entry . '\Gateway';

            if(class_exists($class) && is_subclass_of($class, $parent_class)) {
                $gateway = new $class;
                $this->gateways[$gateway->get_id()] = $gateway;
            }
        });
    }


    public function gform_after_submission($entry, $form) {
        if($form_setup = FormSetup::get_from_form_id($form['id'])) {
            do_action('after_quiz_form_submission', $form_setup, $entry, $form);
        }
    }

    public function gform_confirmation($confirmation, $form, $entry, $is_ajax) {
        if(
            ($form_setup = FormSetup::get_from_form_id($form['id']))
                && $form_setup->meta->survey_result_page_redirect
        ) {
            return [
                'redirect' => $form_setup->get_quiz_result_page_url($entry['id'])
            ];
        }

        return $confirmation;
    }

    public function gform_custom_merge_tags($merge_tags, $form_id, $fields, $element_id) {
        if($form_setup = FormSetup::get_from_form_id($form_id)) {
            $merge_tags[] = [
                'label' => 'Quiz Result Page',
                'tag'   => '{quiz_result_page}'
            ];
        }

        return $merge_tags;
    }

    public function gform_replace_merge_tags($text, $form, $entry, $url_encode, $esc_html, $n2br, $format) {
        if(is_array($form)) {
            $tag = '{quiz_result_page}';
            $form_setup = FormSetup::get_from_form_id($form['id']);

            if($form_setup && strpos($text, $tag) !== false) {
                $text = str_replace(
                    $tag, $form_setup->get_quiz_result_page_url($entry['id']), $text
                );
            }
        }

        return $text;
    }
}

function plugin() {
	return Plugin::getInstance();
}

plugin();
