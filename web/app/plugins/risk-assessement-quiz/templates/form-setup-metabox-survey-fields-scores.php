<?php
$inner_row_template = function($i, $field_id, $choices, $data = []) {
    ?>
    <div class="repeatable-item repeatable-inner-item">
        <div class="item-content-wrap">
            If choice 
            <select name="survey_conditional_messages[<?= $field_id ?>][choice_equals_not][<?= $i ?>][]">
                <option value="equals" <?php selected('equals', isset($data['choice_equals_not']) ? $data['choice_equals_not'] : '') ?>>
                    Equals
                </option>
                <option value="not_equals" <?php selected('not_equals', isset($data['choice_equals_not']) ? $data['choice_equals_not'] : '') ?>>
                    Not equals
                </option>
            </select>
            <select name="survey_conditional_messages[<?= $field_id ?>][choice][<?= $i ?>][]">
                <?php foreach($choices as $choice) : ?>
                    <option value="<?= $choice['value'] ?>" <?php selected($choice['value'], isset($data['choice']) ? $data['choice'] : '') ?>>
                        <?= $choice['text'] ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>

        <div class="item-btns-wrap">
            <button class="add inner-add" type="button">Add</button>
            <button class="remove inner-remove" type="button">Remove</button>
        </div>        
    </div>
    <?php
};//print_r($survey_conditional_messages);

$row_template = function($i, $field_id, $choices, $data = []) use(&$inner_row_template) {
    ?>
    <div class="repeatable-item repeatable-outer-item" data-index="<?= $i ?>">
        <div class="item-content-wrap">
            <div class="match-any-all">
                Match 
                <select name="survey_conditional_messages[<?= $field_id ?>][match_any_all][<?= $i ?>]">
                    <option value="any" <?php selected('any', isset($data['match_any_all']) ? $data['match_any_all'] : '') ?>>
                        Any
                    </option>
                    <option value="all" <?php selected('all', isset($data['match_any_all']) ? $data['match_any_all'] : '') ?>>
                        All
                    </option>
                </select> 
                Conditions:
            </div>

            <div class="match-conditions">
                <?php
                    $options = [
                        'item'          => '.repeatable-inner-item',
                        'min'           => 1,
                        'max'           => count($choices),
                        'btnAdd'        => '.inner-add',
                        'btnRemove'     => '.inner-remove'
                    ]
                ?>
                <div class="repeatable-conditions" data-repeatable='<?= json_encode($options) ?>'>
                    <?php 
                        if(!empty($data['conditions'])) : 
                            foreach($data['conditions'] as $condition) :
                                $inner_row_template($i, $field_id, $choices, $condition);
                            endforeach;
                        else : 
                            $inner_row_template($i, $field_id, $choices);
                        endif;
                    ?>
                </div>
            </div>

            <div class="msg-wrap">
                Then show message:
                <?php 
                    /* wp_editor(
                        $data['message'], 
                        'survey_conditional_messages_'.$field_id.'_message_'.$i,
                        [
                            'textarea_name' => 'survey_conditional_messages['.$field_id.'][message]['.$i.']'
                        ]
                    ) */
                ?>
                <textarea name="survey_conditional_messages[<?= $field_id ?>][message][<?= $i ?>]"><?= $data['message'] ?></textarea>
            </div>
        </div>

        <div class="item-btns-wrap">
            <button class="add outer-add" type="button">Add</button>
            <button class="remove outer-remove" type="button">Remove</button>
        </div>
    </div>
    <?php
};
?>
<div id="survey-fields-scores">
    <div class="fields-row">
        <div class="field-label">
            <label><?php _e('Scores', $textdomain) ?></label>
        </div>
        <div class="field-input">
            <?php foreach($fields as $field) : ?>
                <div class="fields-row fields-row-vertical">
                    <div class="field-label">
                        <label><?= $field->label ?></label>
                    </div>
                    <div class="field-input">
                        <div class="survey-scores-wrap">
                            <ul class="survey-choices">
                                <?php foreach($field->choices as $i => $choice) : ?>
                                    <li>
                                        <div class="survey-choice-wrap">
                                            <?= $choice['text'] ?> 
                                            <input 
                                                type="text" 
                                                class="smalltext" 
                                                name="survey_scores[<?= $field->id ?>][<?= $choice['value'] ?>]" 
                                                value="<?= isset($meta['survey_scores'][$field->id][$choice['value']]) ? $meta['survey_scores'][$field->id][$choice['value']] : '' ?>">
                                        </div>
                                    </li>
                                <?php endforeach ?>
                            </ul>

                            <div class="fields-row fields-row-vertical">
                                <div class="field-label">
                                    <label for="survey_diagram_labels_<?= $field->id ?>"><?php _e('Diagram label') ?></label>
                                </div>
                                <div class="field-input">
                                    <textarea 
                                        id="survey_diagram_labels_<?= $field->id ?>"
                                        name="survey_diagram_labels[<?= $field->id ?>]" 
                                        class="large-text"><?= isset($meta['survey_diagram_labels'][$field->id]) ? $meta['survey_diagram_labels'][$field->id] : '' ?></textarea>
                                </div>
                            </div>

                            <div class="survey-conditional-messages">
                                <?php
                                $options = [
                                    'btnOuterAdd'           => '.outer-add',
                                    'btnOuterAddContext'    => 'within',
                                    'itemTemplate'          => '#survey-conditional-message-tmpl-'.$field->id,
                                    'item'                  => '.repeatable-outer-item',
                                    'min'                   => 0,
                                    'btnAdd'                => '.outer-add',
                                    'btnRemove'             => '.outer-remove',
                                    'itemsContainer'        => '.items'
                                ]
                                ?>
                                <div class="repeatable" data-repeatable='<?= json_encode($options) ?>'>
                                    <div class="items">
                                        <?php
                                            if(!empty($survey_conditional_messages[$field->id])) :
                                                foreach($survey_conditional_messages[$field->id] as $i => $data) :
                                                    $row_template($i, $field->id, $field->choices, $data);
                                                endforeach;
                                            endif;
                                        ?>
                                    </div>
                                    <div class="outer-add-wrap">
                                        <button class="outer-add button" type="button">Add Conditional Message</button>
                                    </div>
                                </div>
                                <script type="text/html" id="survey-conditional-message-tmpl-<?= $field->id ?>">
                                    <?= $row_template(0, $field->id, $field->choices) ?>
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>