<div class="quiz-metabox-container" id="form-setup-metabox" data-post-id="<?= $post->ID ?>">
    <div class="fields-row">
        <div class="field-label">
            <label for="form_id"><?php _e('Gravity Form', $textdomain) ?></label>
        </div>
        <div class="field-input">
            <select name="form_id" id="form_id" class="regular-text">
                <option value><?= __('-Select a form-', $textdomain) ?></option>
                <?php foreach($forms as $form) : ?>
                    <option value="<?= $form->id ?>" <?php selected($form->id, $meta['form_id']) ?>>
                        <?= $form->title ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>
    
    <div id="form-setup-additional-fields">
        <?= $additional_fields ?>
    </div>
</div>