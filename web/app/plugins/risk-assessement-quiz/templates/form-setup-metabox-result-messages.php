<?php
$row_template = function($min = '', $max = '', $message = '') {
    ?>
    <div class="result-message-item repeatable-item">
        <div class="lines-wrap item-content-wrap">
            <div class="line line1">
                If scores between (inclusive) 
                <div class="line-inputs">
                    <input type="text" name="survey_results[min_score][]" value="<?= $min ?>"> 
                    and <input type="text" name="survey_results[max_score][]" value="<?= $max ?>">
                </div>
            </div>
            <div class="line line2 msg-wrap">
                Then show message:
                <textarea name="survey_results[message][]"><?= $message ?></textarea>
            </div>
        </div>
        <div class="btns-wrap">
            <button class="add">Add</button>
            <button class="remove">Remove</button>
        </div>
    </div>
    <?php
};
?>
<div class="fields-row">
    <div class="field-label">
        <label for="survey_result_page"><?php _e('Result Page', $textdomain) ?></label>
    </div>
    <div class="field-input">
        <div class="fields-row fields-row-vertical">
            <div class="field-input">
                <?php 
                    wp_dropdown_pages([
                        'name'      => 'survey_result_page',
                        'id'        => 'survey_result_page',
                        'selected'  => $survey_result_page
                    ])
                ?>
            </div>
        </div>
        
        <div class="fields-row fields-row-vertical">
            <div class="field-input">
                <label>
                    <input type="checkbox" name="survey_result_page_redirect" value="1" <?php checked(1, $survey_result_page_redirect) ?>>
                    <?php _e('Redirect to result page after quiz completion', $textdomain) ?>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="fields-row">
    <div class="field-label">
        <label><?php _e('Result page messages (total score based)', $textdomain) ?></label>
    </div>
    <div class="field-input">
        <div class="fields-row fields-row-vertical">
            <div class="field-label">
                <label><?php _e('Score based messages', $textdomain) ?></label>
            </div>
            <div class="field-input">
                <?php
                $options = [
                    'btnOuterAdd'           => '.outer-add',
                    'btnOuterAddContext'    => 'within',
                    'itemTemplate'          => '#result-message-item',
                    'item'                  => '.result-message-item',
                    'min'                   => 0,
                    'itemsContainer'        => '.items'
                ]
                ?>
                <div id="result-messages" data-repeatable='<?= json_encode($options) ?>' class="repeatable">
                    <div class="items">
                        <?php if(!empty($survey_results['min_score'])) : ?>
                            <?php 
                            foreach($survey_results['min_score'] as $i => $min) : 
                                $max = $survey_results['max_score'][$i];
                                $message = $survey_results['message'][$i];
                                $row_template($min, $max, $message);
                            endforeach;
                            ?>
                        <?php endif ?>    
                    </div>
                    <div class="outer-add-wrap">
                        <button class="outer-add button">Add Row</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/html" id="result-message-item">
        <?php $row_template() ?>
    </script>
</div>
