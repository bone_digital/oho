<?php
namespace balonka\quiz;

use RGFormsModel;
use GFAPI;

function inject($string, array $vars) {
    return preg_replace_callback(
        '/\{([^}]+)\}/', function($matches) use($vars) {
            $var = $matches[1];
            if(array_key_exists($var, $vars)) {
                return $vars[$var];
            }

            return $matches[0];
        }, $string
    );
}

function load_wordpress() {
    $dir = __DIR__;
    do {
        $wp_load = "$dir/wp-load.php";
        if(is_file($wp_load)) {
            define('WP_USE_THEMES', false);
		    require $wp_load;
            break;
        }

        $dir = dirname($dir);
    } while($dir != '.');
}

function get_gravity_forms() {
    if(class_exists(RGFormsModel::class)) {
        return RGFormsModel::get_forms();
    }

    return [];
}

function get_gravity_form_fields($form_id, $type = '', $type_equals_not = 'equals') {
    $fields = [];
    $types = is_array($type) ? $type : [$type];

    if(class_exists(RGFormsModel::class)) {
        $form_meta = RGFormsModel::get_form_meta($form_id);
        if(!empty($form_meta) && !is_wp_error($form_meta) && !empty($form_meta['fields'])) {
            foreach($form_meta['fields'] as $field) {
                $match = empty($types);
                $match = $match || $type_equals_not == 'equals' ? in_array($field->type, $types) : !in_array($field->type, $types);
                if($match) {
                    $fields[$field->id] = $field;
                }
            }
        }
    }

    return $fields;
}

function get_gravity_form_entry($entry_id) {
    if(class_exists(GFAPI::class)) {
        $entry = GFAPI::get_entry($entry_id);
        if(!is_wp_error($entry)) {
            return $entry;
        }
    }
}
