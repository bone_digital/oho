(function($, config) {
    var quiz = {
        saveSettings: function(data) {
            return $.ajax(config.ajax_url, {
                method: 'POST',
                dataType: 'json',
                data: {
                    action: config.save_settings,
                    data: data
                }
            }).then(function(result) {
                if(!result.success) {
                    return $.Deferred().reject();
                }
            });
        },

        loadSurveyScoresFields: function(post_id, form_id) {
            return $.ajax(config.ajax_url, {
                method: 'POST',
                /* dataType: 'json', */
                data: {
                    action: 'load_form_survey_fields_scores',
                    post_id: post_id,
                    form_id: form_id
                }
            })/* .then(function(result) {
                if(!result.success) {
                    return $.Deferred().reject();
                }
            }) */;
        },

        setSurveyConditionalMessageIndex: function(e, $item, $clone) {
            var $prev = $clone.prev();
            var index = $prev.length ? parseInt($prev.data('index')) : -1;
            var newIndex = (isNaN(index) ? -1 : index) + 1;

            $clone.data('index', newIndex);
            $clone.find('select, textarea, input').each(function() {
                this.name = this.name.replace(/\[\d+\]$/, '[' +newIndex+ ']');
                this.name = this.name.replace(/\[\d+\]\[\]$/, '[' +newIndex+ '][]');
            })
        },

        event: {
            onSubmitSettings: function(e) {
                e.preventDefault();

                var $form = $(this),
                    data = $form.serialize(),
                    savingMsg = $form.data('savingMsg') || 'Saving...',
                    successMsg = $form.data('successMsg') || 'Successfully saved',
                    errorMsg = $form.data('errorMsg') || 'Error saving!',
                    $submit = $form.find('[type=submit]');

                $submit.data('text', $submit.text()).text(savingMsg).prop('disabled', true);

                donation.saveSettings(data)
                    .done(function() {
                        $submit.after('<span class="save_status success-msg" style="margin-left: 10px;color: green">' +successMsg+ '</span>');
                    })
                    .fail(function() {
                        $submit.after('<span class="save_status error_msg" style="margin-left: 10px;color: red">' +errorMsg+ '</span>');
                    })
                    .always(function() {
                        $submit.text(
                            $submit.data('text')
                        ).prop('disabled', false);

                        var t = setTimeout(function() {
                            $submit.next('span').fadeOut(400, function() {
                                $submit.next('span').remove();
                            });
                        }, 7000)
                    })
            },

            onSelectForm: function(e) {
                var $formMetaBox = $('#form-setup-metabox');
                var $surveyFields = $('#form-setup-additional-fields');
                var form_id = $(this).val();
                var post_id = $formMetaBox.data('postId');
                
                var loadingMsg = $surveyFields.data('loadingMsg') || 'Loading...';
                var errorMsg = $surveyFields.data('errorMsg') || 'Error loading!';

                $surveyFields.html(loadingMsg);

                quiz.loadSurveyScoresFields(post_id,  form_id)
                    .done(function(result) {
                        $surveyFields.html(result);
                        $surveyFields.find('[data-repeatable]').repeatable();
                        $formMetaBox.trigger('selectedForm', form_id);
                    })
                    .fail(function() {
                        $surveyFields.html(errorMsg);
                    })
            },

            onToggle: function(e) {
                var $chk = $(this), checked = this.checked;
                var $toggle = $($chk.data('toggle'));
                var toggleCheckedState = $chk.data('toggleCheckedState') || 'show';
                var slideDuration = 400, fn;

                if(toggleCheckedState == 'show') {
                    fn = checked ? 'slideDown' : 'slideUp';
                } else {
                    fn = checked ? 'slideUp' : 'slideDown';
                }

                $toggle[fn](slideDuration);
            }
        }
    };

    $(function() {
        //$('#donation-main').on('submit', '.donation-settings form', donation.event.onSubmitSettings);
        $('#form-setup-metabox').on('change', 'select[name=form_id]', quiz.event.onSelectForm);
        //$('#gateway-metabox').on('change', 'input[type=checkbox]', donation.event.onToggle);

        $('#survey-fields-scores .survey-conditional-messages > .repeatable').on('afterAdd', quiz.setSurveyConditionalMessageIndex);


        $('.reveal').on('click', '.btn-reveal, .block', function(e) {
            var $reveal = $(this).closest('.reveal');
            var reveal = $reveal.data('reveal') || 'full';
            var $target = $(e.target);

            if(reveal == 'partial') {
                $reveal.attr('data-reveal', 'full');
                $reveal.data('reveal', 'full');
            } else if($target.is('.btn-reveal')) {
                $reveal.attr('data-reveal', 'partial');
                $reveal.data('reveal', 'partial');
            }
        })
    })
})(jQuery, window.quiz_config || {});