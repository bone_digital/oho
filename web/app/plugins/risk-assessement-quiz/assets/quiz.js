(function($) {
    $(function() {
        $('canvas[data-chart-data]').each(function() {
            var $canvas = $(this),
                labels = $canvas.data('chartLabels'),
                data = $canvas.data('chartData'),
                caption = $canvas.data('chartCaption');

            /* if(data.indexOf(0) == -1) {
                data.unshift(0);
            }

            if(data.indexOf(100) == -1) {
                data.push(100);
            } */


            new Chart(this, {
                "type": "radar",
                "data": {
                    "labels": labels,
                    "datasets": [{
                        "label": caption,
                        "data": data,
                        "fill": 'shape',
                        //"backgroundColor": "rgba(255, 99, 132, 0.2)",
                        "backgroundColor": "rgb(67, 115, 46,0.2)",
                        //"borderColor": "rgb(255, 99, 132)",
                        "borderColor": "#325826",
                        "pointBackgroundColor": "rgb(255, 99, 132)",
                        "pointBorderColor": "#fff",
                        "pointHoverBackgroundColor": "#fff",
                        "pointHoverBorderColor": "rgb(255, 99, 132)",
                        'pointRadius': 5,
                        'pointHoverRadius': 5,
                        pointHitRadius: 5,
                    }/* , {
                        "label": "My Second Dataset",
                        "data": [28, 48, 40, 19, 96, 27, 100],
                        "fill": true,
                        "backgroundColor": "rgba(54, 162, 235, 0.2)",
                        "borderColor": "rgb(54, 162, 235)",
                        "pointBackgroundColor": "rgb(54, 162, 235)",
                        "pointBorderColor": "#fff",
                        "pointHoverBackgroundColor": "#fff",
                        "pointHoverBorderColor": "rgb(54, 162, 235)"
                    } */]
                },
                "options": {
                    "elements": {
                        "line": {
                            "tension": 0,
                            "borderWidth": 3
                        }
                    },

                    scales: {
                        r: {
                            suggestedMin: -11,
                            suggestedMax: 1,
                            pointLabels: {
                                font: {
                                    size: 12
                                }
                            }
                        },
                    },

                    plugins: {
                        legend: {
                            labels: {
                                // This more specific font property overrides the global property
                                font: {
                                    size: 14
                                }
                            }
                        },
                    }
                }
            });            
        });
       
    })
})(jQuery);