(function($) {
	$.fn.repeatable = function() {
		var defaults = {
			min: 1,
			max: 0,
			btnAdd: '.add',
			btnOuterAdd: null,
            btnOuterAddContext: 'global',
			btnRemove: '.remove',
			item: '> div',
			itemTemplate: '',
            itemsContainer: ''
		}, args = arguments;
		
		this.each(function() {
			var $this = $(this), 
				dataOptions = $this.data('repeatable') || {},
				options = $.extend({},
					defaults, 
					typeof args[0] == 'object' ? args[0] : {}, 
					typeof dataOptions == 'object' ? dataOptions : {}
				), min, max,
				$btnOuterAdd = options.btnOuterAddContext == 'global' 
                                ? $(options.btnOuterAdd)
                                : $this.find(options.btnOuterAdd);
			
			options.min = isNaN(min = parseInt(options.min)) ? 1 : min;
			options.max = isNaN(max = parseInt(options.max)) ? 0 : max;
			
			function clone($item) {
				var $clone = options.itemTemplate 
						? $($(options.itemTemplate).html())
						: $item.clone();

                $clone.find('[data-repeatable]').repeatable();

                return $clone;
			}
			
			if(options.itemTemplate)
				$btnOuterAdd.click(function(e) {
					e.preventDefault();
					
					var $clone = clone();
					
					$this.trigger('beforeAdd', [null, $clone, $btnOuterAdd, getItemsCount()]);
					(options.itemsContainer ? $this.find(options.itemsContainer) : $this).append($clone);
					$this.trigger('afterAdd', [null, $clone, $btnOuterAdd, getItemsCount()]);
					
					$clone.find('input, textarea').val('').prop('disabled', false);
					checkMinMax();

                    return false;
				});
			
			$this.on('click', options.btnAdd, function(e) {
				e.preventDefault();
				
				var $btn = $(this), 
					$item = $btn.closest(options.item),
					$clone = clone($item);
				
				$this.trigger('beforeAdd', [$item, $clone, $btnOuterAdd, getItemsCount()]);
				$item.after($clone);
				$this.trigger('afterAdd', [$item, $clone, $btnOuterAdd, getItemsCount()]);
				
				$clone.find('input, textarea').val('');
				checkMinMax();

                return false;
			});
			
			$this.on('click', options.btnRemove, function(e) {
				e.preventDefault();
				
				var $btn = $(this), 
					$item = $btn.closest(options.item);
				
				$this.trigger('beforeRemove', [$item, $btn, $btnOuterAdd, getItemsCount()]);
				$item.remove();
				$this.trigger('afterRemove', [$item, $btn, $btnOuterAdd, getItemsCount()]);
				
				checkMinMax();

                return false;
			});
			
			function checkMinMax() {
				var len = getItemsCount(),
					$add = $this.find(options.btnAdd),
					$remove = $this.find(options.btnRemove);
				
				$add.prop('disabled', false);
				$btnOuterAdd.prop('disabled', false);
				$remove.prop('disabled', false);
				
				if(options.max > 0 && len >= options.max) {
					$add.prop('disabled', true);
					$btnOuterAdd.prop('disabled', true);
				}
					
				if(len <= options.min)
					$remove.prop('disabled', true);
			}
			
			function getItemsCount() {
				return $this.find(options.item).length;
			}
			
			checkMinMax();
		});
	}
	
	$(document).ready(function() {
		$('[data-repeatable]').repeatable();
	});
})(jQuery);