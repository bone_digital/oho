<?php
namespace balonka\quiz;

use balonka\quiz\Plugin;
use balonka\quiz\PostType;

class TaxonomyType {
    protected $type;
    protected $labels = [];
    protected $args = [];
    protected $post_type;
    protected $label_singular;
    protected $label_plural;
    protected $filter_posts = false;
    protected $select_option_all;
    protected $parent_menu;

    public function __construct($type, $label_singular = '', $label_plural = '', array $args = [], array $labels = []) {
        $this->type = $type;
        $this->label_singular = $label_singular;
        $this->label_plural = $label_plural;

        $this->set_args($args);
        $this->set_labels($labels);

        add_action('init', function() {
            $this->init();
        });

        add_action('admin_menu', function() {
            $this->register_submenu();
        });

        add_filter('parent_file', [$this, 'set_current_submenu']);
    }

    public function get_type() {
        return $this->type;
    }

    public function get_args() {
        return $this->args;
    }

    public function set_args(array $args) {
        $default_args = [
            'public'             	=> true,
            'show_ui'            	=> true,
            'show_in_menu'       	=> true,
            'show_tagcloud'			=> false,
            'show_admin_column'		=> true,
            'hierarchical'			=> true,
            'query_var'				=> true,
        ];

        $this->args = array_replace_recursive($default_args, $this->args, $args);
    }

    public function get_labels() {
        return $this->labels;
    }

    public function set_labels(array $labels) {
        $singular = $this->label_singular ? $this->label_sinular : ucfirst($this->type);
        $plural = $this->label_plural ? $this->label_plural : $singular.'s';

        $default_labels = [
            'name'              => _x($plural, 'taxonomy general name', Plugin::TEXT_DOMAIN),
            'singular_name'     => _x($singular, 'taxonomy singular name', Plugin::TEXT_DOMAIN),
            'edit_item'         => __('Edit '.$singular, Plugin::TEXT_DOMAIN),
            'update_item'       => __('Update '.$singular, Plugin::TEXT_DOMAIN),
            'add_new_item'      => __('Add New '.$singular, Plugin::TEXT_DOMAIN),
            'new_item_name'     => __("New $singular Name", Plugin::TEXT_DOMAIN),
            'menu_name'         => __($plural, Plugin::TEXT_DOMAIN),
            'search_items'      => __('Search '.$plural, Plugin::TEXT_DOMAIN),
            'all_items'         => __('All '.$plural, Plugin::TEXT_DOMAIN),
            'parent_item'       => __('Parent '.$singular, Plugin::TEXT_DOMAIN),
            'parent_item_colon' => __("Parent $singular:", Plugin::TEXT_DOMAIN),
            'separate_items_with_commas' => __("Separate $plural_lower with commas", Plugin::TEXT_DOMAIN),
            'choose_from_most_used' => __('Choose from the most used '.$plural_lower, Plugin::TEXT_DOMAIN),
            'not_found'			=> __("No $plural_lower found.", Plugin::TEXT_DOMAIN)
        ];

        $this->labels = array_replace_recursive($default_labels, $this->labels, $labels);
    }

    public function get_post_type() {
        return $this->post_type;
    }

    public function set_post_type(PostType $post_type) {
        $this->post_type = $post_type;
    }

    public function get_parent_menu() {
        return $this->parent_menu;
    }

    public function set_parent_menu($parent_menu) {
        $this->parent_menu = $parent_menu;
    }

    protected function init() {
        $args = $this->args;
        $args['labels'] = $this->labels;
        
        register_taxonomy(
            $this->type, $this->post_type ? $this->post_type->get_type() : null, $args
        );

        if(!empty($this->post_type)) {
            register_taxonomy_for_object_type(
                $this->get_type(), $this->post_type->get_type()
            );
        }
    }

    public function filter_listing() {
        if(func_num_args() > 0) {
            $this->filter_posts = (bool) func_get_arg(0);
        } else {
            return $this->filter_posts;
        }
    }

    protected function register_submenu() {
        if($this->parent_menu) {
            $parent_menu = $this->parent_menu;
        } elseif($this->post_type) {
            $parent_menu = $this->post_type->get_parent_menu();
        }

        if(isset($parent_menu)) {
            $menu_name = isset($this->labels['menu_name']) 
                            ? $this->labels['menu_name'] : $this->labels['name'];
                            
            add_submenu_page(
                $parent_menu,
                $labels['name'],
                $menu_name,
                'edit_posts',
                sprintf(
                    'edit-tags.php?taxonomy=%s&post_type=%s',
                    $this->get_type(), $this->post_type ? $this->post_type->get_type() : ''
                )
            );
        }
    }

    public function set_current_submenu($parent_file) {
        global $submenu_file, $current_screen, $pagenow;

        if(
            $this->post_type && 
            ($parent_menu = $this->post_type->get_parent_menu()) && 
            $current_screen->post_type == $this->post_type->get_type()
        ) {
            if ($pagenow == 'edit-tags.php') {
                $submenu_file = sprintf(
                    'edit-tags.php?taxonomy=%s&post_type=%s',
                    $this->get_type(), $this->post_type->get_type()
                );
            }

            $parent_file = $parent_menu;

        }

        return $parent_file;
    }
}