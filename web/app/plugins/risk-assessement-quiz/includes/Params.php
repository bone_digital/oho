<?php
namespace balonka\quiz;

class Params implements \Iterator, \ArrayAccess {
    protected $data = [];
    protected $defs =  [];

    public function __construct(array $defs = []) {
        $this->defs = $defs;
        empty($defs) or $this->import([]);
    }

    public function set($key, $value) {
        if(empty($this->defs) || isset($this->defs[$key])) {
            if(isset($this->defs[$key])) {
                $def = $this->defs[$key];
                if(is_array($def) && isset($def['parser']) && is_callable($def['parser'])) {
                    $value = call_user_func($def['parser'], $value);
                }
            }

            $this->data[$key] = $value;
        }
    }

    public function get($key) {
        if(array_key_exists($key, $this->data))
            return $this->data[$key];
    }

    public function import($data) {
        $data = (array) $data;

        if(!empty($this->defs))  {
            $this->data = [];
            foreach($this->defs as $key => $def) {
                is_array($def) or $def = ['default' => $def];

                if(array_key_exists($key, $data)) {
                    $value = $data[$key];
                } elseif(array_key_exists('default', $def)) {
                    $value = $def['default'];
                }

                if(isset($value)) {
                    $this->set($key, $value);
                    unset($value);
                }
            }
        } else {
            $this->data = $data;
        }
    }

    public function export() {
        return $this->data;
    }

    public function current() {
        return current($this->data);
    }

    public function key() {
        return key($this->data);
    }

    public function next() {
        return next($this->data);
    }

    public function rewind() {
        return reset($this->data);
    }

    public function valid() {
        return key($this->data) !== null;
    }

    public function offsetExists($key) {
        return array_key_exists($key, $this->data);
    }

    public function offsetGet($key) {
        if(isset($this->data[$key]))
            return $this->data[$key];
    }

    public function offsetSet($key, $value) {
        if(empty($this->defs) || isset($this->defs[$key]))
            $this->data[$key] = $value;
    }

    public function offsetUnset($key) {
        if(isset($this->data[$key]))
            unset($this->data[$key]);
    }

    public function __get($key) {
        return $this->get($key);
    }

    public function __set($key, $value) {
        $this->set($key, $value);
    }

    public function __isset($key) {
        return isset($this->data[$key]);
    }

    public function __unset($key) {
        unset($this->data[$key]);
    }
}