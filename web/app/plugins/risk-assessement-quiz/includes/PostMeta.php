<?php 
namespace balonka\quiz;

class PostMeta {
    protected $post_id;
    protected $prefix;

    public function __construct($post_id, $prefix = '') {
        $this->post_id = $post_id;
        $this->prefix = $prefix;
    }

    public function set_prefix($prefix) {
        $this->prefix = $prefix;
    }

    public function add($meta_name, $meta_value) {
        return add_post_meta(
            $this->post_id, $this->get_meta_key_name($meta_name), $meta_value
        );
    }

    public function update($meta_name, $meta_value) {
        return update_post_meta(
            $this->post_id, $this->get_meta_key_name($meta_name), $meta_value
        );
    }

    public function delete($meta_name) {
        foreach(func_get_args() as $meta_name) {
            return delete_post_meta(
                $this->post_id, $this->get_meta_key_name($meta_name)
            );
        }
    }
    
    public function get($meta_name, $single = true) {
        return get_post_meta(
            $this->post_id, $this->get_meta_key_name($meta_name), $single
        );
    }

    public function __get($name) {
        if($name == 'post_id') {
            return $this->post_id;
        }

        return $this->get($name);
    }

    public function __set($name, $value) {
        $this->update($name, $value);
    }

    protected function get_meta_key_name($name) {
        return $this->prefix . $name;
    }
}