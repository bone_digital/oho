<?php
namespace balonka\quiz;

class Logger {
    protected static $default_id = 'global';
    protected static $defaults = [];
    protected static $logs = [];
    protected function __construct() {}

    public static function set_default_id($id) {
        !$id or static::$default_id = $id;
    }

    public static function set_defaults(array $defaults) {
        $id = static::$default_id;
        !empty(static::$defaults) or static::$defaults = [
            'mode'      => 'a',
            'max_logs'  => 3,
            'header'    => "$id logs",
            'footer'    => 'End of log',
            'save_path' => __DIR__ . '/logs',
            'log_to'    => 'file',
            'type'      => 'id_dir'
        ];

        static::$defaults = array_merge(static::$defaults, $defaults);
    }

    public static function get($id = '') {
        $id or $id = static::$default_id;
        static::set_config($id, []);

        if(!isset(static::$logs[$id]['object'])) {
            static::$logs[$id]['object'] = static::get_log(
                $id, static::$logs[$id]['config']
            );
        }

        static::register_shutdown();
        
        return static::$logs[$id]['object'];
    }

    public static function set_config($id, array $config) {
        static::set_defaults([]);
        $id or $id = static::$default_id;

        static::$logs[$id]['config'] = array_merge(
            static::$defaults, 
            ['header' => "$id logs"],
            isset(static::$logs[$id]['config']) ? static::$logs[$id]['config'] : [], 
            $config
        );

        if(!empty($config) && isset(static::$logs[$id]['object'])) {
            unset(static::$logs[$id]['object']);
        }
    }

    private static function get_log($id, $config) {
        return function($line) use($id, $config) {
            static $fp;

            $now = date('Y-m-d H:i:s');
            $today = date('Y-m-d');
            $lines = [];

            $is_cli = function() {
                return !isset($_SERVER['SERVER_SOFTWARE']) && (
                    php_sapi_name() == 'cli' || (is_numeric($_SERVER['argc']) && $_SERVER['argc'] > 0)
                );
            };

            $log_to = function($key) use(&$config) {
                return in_array(
                    $key, preg_split('/\s+/', $config['log_to'])
                );
            };

            $out = function($lines) use(&$fp, &$log_to, &$is_cli) {
                if(is_array($lines)) {
                    $lines = implode("\n", $lines);
                }

                $lines .= "\n";

                if($log_to('file')) {
                    fwrite($fp, $lines, strlen($lines));
                }
                
                if($log_to('stdout')) {
                    if($is_cli()) {
                        echo $lines;
                    } else {
                        echo "<pre>$lines</pre>";
                        ob_flush();
                        flush();
                    }
                }
            };

            if(!$fp) {
                if($log_to('file')) {
                    $save_path = $config['save_path'] . ($config['type'] == 'id_dir' ? "/$id" : '');
                    is_dir($save_path) 
                        or mkdir($save_path, 0755, true);

                    $file = sprintf(
                        '%s/%s.log', $save_path, $config['type'] == 'id_dir' ? $today : $id
                    );

                    $fp = fopen($file, $config['mode']);

                    if(!$fp) {
                        return false;
                    }
                } else {
                    $fp = true;
                }
                
                $lines[] = '# ' . $config['header'];
                $lines[] = '#------------------------------------------------------------------';
            }
            
            if(strtoupper($line) == '%EOL%') {
                $lines[] = "[$now] " . $config['footer'];
                $lines[] = '#------------------------------------------------------------------';
                $out($lines);

                if($log_to('file')) {
                    fclose($fp);
                    unset($fp);
                }
            } else {
                $args = [];
                for($i = 1; $i < func_num_args(); $i++) {
                    $arg = func_get_arg($i);
                    switch(true) {
                        case is_bool($arg):
                            $arg = $arg ? 'true' : 'false';
                            break;
                            
                        case is_array($arg):
                        case is_object($arg):
                            $arg = print_r($arg, true);
                            break;
                            
                        case is_resource($arg):
                            $arg = 'resource#' . get_resource_type($arg);
                    }
                    
                    $args[] = $arg;
                }

                empty($args) or $line = vsprintf($line, $args);
                $line = (preg_match('/^\s*$/', $line) ? '' : "[$now] ") . $line;
                $lines[] = $line;
                $out($lines);
            }
        };
    }

    private static function register_shutdown() {
        static $registered = false;

        $registered or register_shutdown_function(function() {
            foreach(static::$logs as $id => $data) {
                $config = $data['config'];
                if($config['type'] !== 'id_dir') continue;

                $save_path = $config['save_path'] . "/$id";
                $dir = @dir($save_path);
                $files = [];

                if(!$dir) return;

                while(false != ($entry = $dir->read())) {
                    if(!preg_match('/^(\d{4}-\d{2}-\d{2})\.log$/', $entry, $matches))
                        continue;

                    list(, $date) = $matches;
                    $files[strtotime($date)] = $save_path .'/'. $entry;
                }

                $dir->close();

                if(count($files) > $config['max_logs']) {
                    krsort($files);
                    array_splice($files, 0, $config['max_logs']);

                    foreach($files as $file) {
                        unlink($file);
                    }
                }
            }
        });

        $registered = true;
    }
}
