<?php
namespace balonka\quiz\shortcodes\Quiz_Graph;

use balonka\quiz\Shortcode;
use balonka\quiz\Params;
use balonka\quiz\FormSetup;

class Quiz_Graph extends Shortcode {
    public function get_tag() {
        return 'quiz_graph';
    }

    protected function output($atts, $content) {
        $params = new Params([
            'form_id'       => (int)$_GET['form_id'],
            'entry_id'      => (int)$_GET['entry_id'],
            'token'         => $_GET['token'],
            'caption'       => 'Score',
        ]);

        $params->import($atts);

        if($params->form_id && $params->entry_id && $params->token) {
            $form_setup = FormSetup::get_from_form_id($params->form_id);

            if($form_setup->validate_result_token($params->entry_id, $params->token)) {
                $result = $form_setup->get_quiz_result($params->entry_id);

                extract($result);
                $has_conditional_messages = false;

                if(!empty($entries)) {
                    $labels = $data = [];
                    foreach($entries as $entry) {
                        $labels[] = array_merge(
                            explode("\n", $entry['diagram_label']),
                            ['(Score: ' .$entry['score']. ')']
                        );
                        
                        $data[] = $entry['score'];

                        if(!empty($entry['messages'])) {
                            $has_conditional_messages = true;
                        }
                    }

                    include __DIR__ . '/templates/quiz_graph.php';
                }
            }
        }
    }
}