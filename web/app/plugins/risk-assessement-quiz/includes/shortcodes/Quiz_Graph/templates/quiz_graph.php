<div class="quiz-result alignwide">
    <canvas
        class="chartjs"
        data-chart-labels='<?= json_encode($labels) ?>'
        data-chart-data='<?= json_encode($data) ?>'
        data-chart-caption="<?= $params['caption'] ?>"></canvas>

    <div class="quiz-container">
        <div class="quiz-ending-msg"><?= $ending_msg ?></div>

        <?php if($has_conditional_messages) : ?>
            <div class="quiz-guide-working-with-children">
                <h2>Learn more in the FREE Guide for Working with Children!</h2>
                <p>Based on your answers we recommend that you read the following chapters and sections in the Guide for Working with Children chapters:</p>
                <ul>
                    <?php foreach($entries as $entry) : ?>
                        <?php if(!empty($entry['messages'])) : ?>
                            <li><?= implode('</li><li>', $entry['messages']) ?></li>
                        <?php endif ?>
                    <?php endforeach ?>

                    <li>To fully understand the <b>Risks and Penalties for Organisations and employees Working With
    Children</b> read chapter 4 of the “<a href="https://weareoho.com/Free-Working-With-Children-guide">Guide to Compliance for Working With Children Checks</a>”. This
    chapter contains a full state by state overview of employees, board members and other
    stakeholders who need checks and the minimum and maximum penalties for non-compliance. </li>
                </ul>
            </div>
        <?php endif ?>
    </div>
</div>
