<?php
namespace balonka\quiz;

final class Loader {
	private static $callbacks = [];
	private static $map = [
		'namespace' => [],
		'classes'	=> []
	];
	
	private function __construct(){}
	
	public static function init() {
		static $initialized = false;
		
		if(!$initialized) {
            spl_autoload_register(__CLASS__ . '::load');
            $initialized = true;
        }
	}
	
	public static function mapNamespace($ns, $dir) {
		self::$map['namespace']
			[self::toAbsolute($ns, 'namespace')] = self::toAbsolute($dir);
	}
	
	public static function mapClasses($classes, $dir, $namespace = '') {
		is_array($classes) or $classes = preg_split('/\s+/', trim($classes));
		
		foreach($classes as $class)
			self::mapClass($class, $dir, $namespace);
	}
	
	public static function mapClass($class, $dir, $namespace = '') {
		$class = self::toAbsolute($class, 'namespace', $namespace);
		
		try {
			self::$map['classes'][$class] = self::toAbsolute($dir);
		} catch(\InvalidArgumentException $e) {
			if(($rpos = strrpos($class, '\\')) !== false && $rpos !== 0) {
				$namespace = substr($class, 0, $rpos);
				
				if(isset(self::$map['namespace'][$namespace])) {
					$dir = self::toAbsolute(self::$map['namespace'][$namespace] .'/'. $dir);
					self::$map['classes'][$class] = $dir;
			
					return;
				}
			}
			
			throw $e;
		}
	}
		
	public static function toAbsolute($path, $type='filesystem', $defaultNamespace='') {
		switch($type) {
			case 'filesystem':
				$current = getcwd();
				chdir(__DIR__);
				
				if(($abs = realpath($path)) === false)
					throw new \InvalidArgumentException("Path \"$path\" is not a valid path");
				
				$path = $abs;
				chdir($current);
				break;
				
			case 'namespace':
				empty($defaultNamespace) 
					or $defaultNamespace = self::toAbsolute($defaultNamespace, 'namespace');
					
				$path = str_replace('.', '\\', $path);
				
				if($path[0] !== '\\')
					$path = $defaultNamespace .'\\'. $path;
					
				$path = preg_replace('/\\{2,}/', '\\', rtrim($path, '\\'));
		}
		
		return $path;
	}
	
	public static function onLoad($name, $callback) {
		if(!is_callable($callback))
			return;
			
		$name = self::toAbsolute($name, 'namespace');
		
		if(!isset(self::$callbacks[$name]))
			self::$callbacks[$name] = [];
			
		self::$callbacks[$name][] = $callback;
	}
	
	// autoload method
	public static function load($name) {
		$name = self::toAbsolute($name, 'namespace');
		
		if(isset(self::$map['classes'][$name]))
			$paths = [self::$map['classes'][$name]];
			
		elseif(count(($parts = explode('\\', $name))) > 2) {
			$class = array_pop($parts);
			$ns = $parts;
			$paths = [];
			
			while(count($parts) > 0) {
				$namespace = implode('\\', $parts);
				
				if(isset(self::$map['namespace'][$namespace])) {
					$path = self::$map['namespace'][$namespace];
					
					if(count($ns) > ($count = count($parts)))
						$paths[] = $path .'/'. implode('/', array_slice($ns, $count));
						
					$paths[] = $path;
					break;
				}
				
				array_pop($parts);
			}
			
			$paths[] = __DIR__ . implode('/', $ns);
		} else
			$paths = [__DIR__];
		
		isset($class) 
			or $class = current(
				array_slice(explode('\\', $name), -1, 1)
			);
			
		$classLower = strtolower($class);
		$classDot = str_replace('_', '.', $class);
		$classDotLower = strtolower($classDot);
		
		foreach($paths as $path) {
			if(is_file($path)) {
				$file = $path;
				break;
			}
			
			$pattern = '%s/%s.php';
			
			$files = [
				sprintf($pattern, $path, $class),
				sprintf($pattern, $path, $classLower),
			];
			
			if($class != $classDot) {
				$files[] = sprintf($pattern, $path, $classDot);
				$files[] = sprintf($pattern, $path, $classDotLower);
			}
			
			foreach($files as $f) {
				if(is_file($f)) {
					$file = $f;
					break 2;
				}
			}
		}
		
		if(isset($file)) {
			require $file;
			
			if(
				(class_exists($name) or interface_exists($name) or trait_exists($name)) 
					and isset(self::$callbacks[$name])
			)
				foreach(self::$callbacks[$name] as $fn) $fn();
		}
	}
}

loader::init();