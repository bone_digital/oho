<?php
namespace balonka\quiz;

use balonka\quiz\Plugin;
use balonka\quiz\TaxonomyType;

class PostType {
    protected $type;
    protected $labels = [];
    protected $args = [];
    protected $taxonomies = [];
    protected $parent_menu;
    protected $label_singular;
    protected $label_plural;
    protected $metaboxes = [];
    
    
    public function __construct($type, $label_singular = '', $label_plural = '', array $args = [], array $labels = []) {
        $this->type = $type;
        $this->label_singular = $label_singular;
        $this->label_plural = $label_plural;

        $this->set_args($args);
        $this->set_labels($labels);

        add_action('restrict_manage_posts', [$this, 'display_taxonomies_dropdowns']);
        add_action('parse_query', [$this, 'filter_taxonomies']);

        add_action('init', function() {
            $this->init();
        });

        add_action('add_meta_boxes', function() {
            $this->init_metaboxes();
        });

        add_action('save_post_' . $this->type, [$this, 'save_metaboxes']);
    }

    public function get_type() {
        return $this->type;
    }

    public function get_args() {
        return $this->args;
    }

    public function set_args(array $args) {
        $default_args = [
            'public'             	=> true,
            'publicly_queryable' 	=> true,
            'show_ui'            	=> true,
            'show_in_menu'       	=> true,
            'exclude_from_search'   => false,
            'supports'           	=> ['title', 'thumbnail', 'editor', 'excerpt'],
        ];

        $this->args = array_replace($default_args, $this->args, $args);
    }

    public function get_labels() {
        return $this->labels;
    }

    public function set_labels(array $labels) {
        $singular = $this->label_singular ? $this->label_singular : ucfirst($this->type);
        $plural = $this->label_plural ? $this->label_plural : ($singular.'s');

        $default_labels = [
            'name'               => _x($plural, 'post type general name', Plugin::TEXT_DOMAIN),
            'singular_name'      => _x($singular, 'post type singular name', Plugin::TEXT_DOMAIN),
            'menu_name'          => _x($plural, 'admin menu', Plugin::TEXT_DOMAIN),
            'name_admin_bar'     => _x($plural, 'add new on admin bar', Plugin::TEXT_DOMAIN),
            'add_new'            => _x('Add New', 'post', Plugin::TEXT_DOMAIN),
            'add_new_item'       => __('Add New '.$singular, Plugin::TEXT_DOMAIN),
            'new_item'           => __('New '.$singular, Plugin::TEXT_DOMAIN),
            'edit_item'          => __('Edit '.$singular, Plugin::TEXT_DOMAIN),
            'view_item'          => __('View '.$singular, Plugin::TEXT_DOMAIN),
            'all_items'          => __('All '.$plural, Plugin::TEXT_DOMAIN),
            'search_items'       => __('Search '.$plural, Plugin::TEXT_DOMAIN),
            'parent_item_colon'  => __(sprintf('Parent %s:', $plural), Plugin::TEXT_DOMAIN),
            'not_found'          => __(sprintf('No %s found.', $plural), Plugin::TEXT_DOMAIN),
            'not_found_in_trash' => __(sprintf('No %s found in Trash.', $plural), Plugin::TEXT_DOMAIN)
        ];

        $this->labels = array_replace($default_labels, $this->labels, $labels);
    }

    public function get_taxonomies() {
        return $this->taxonomies;
    }

    public function get_parent_menu() {
        return $this->parent_menu;
    }

    public function set_parent_menu($parent_menu) {
        $this->parent_menu = $parent_menu;
    }

    public function add_taxonomy(TaxonomyType $taxonomy) {
        $this->taxonomies[] = $taxonomy;
    }

    public function add_meta_box($id, array $metabox) {
        $this->metaboxes[$id] = $metabox;
    }

    protected function init() {
        $args = $this->args;
        $args['labels'] = $this->labels;

        if(!empty($this->parent_menu)) {
            $args['show_in_menu'] = $this->parent_menu;
        }

        if(!empty($this->taxonomies)) {
            $args['taxonomy'] = [];
            foreach($this->taxonomies as $taxonomy) {
                $args['taxonomy'][] = $taxonomy->get_type();
            }
        }

        register_post_type($this->get_type(), $args);
    }

    protected function init_metaboxes() {
        foreach($this->metaboxes as $id => $metabox) {
            add_meta_box(
                $id, $metabox['title'], $metabox['render'], $this->type,
                isset($metabox['context']) ? $metabox['context'] : 'advanced',
                isset($metabox['priority']) ? $metabox['priority'] : 'default',
                isset($metabox['args']) ? $metabox['args'] : null
            );
        }
    }

    public function save_metaboxes($post_id/* , $post, $update */) {
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }

        foreach($this->metaboxes as $id => $metabox) {
            if(isset($metabox['save']) && is_callable($metabox['save'])) {
                call_user_func_array(
                    $metabox['save'], func_get_args()
                );
            }
        }
    }

    public function display_taxonomies_dropdowns() {
        global $typenow;

        if($this->get_type() != $typenow)
			return;

        foreach($this->get_taxonomies() as $taxonomy) {
			if(!$taxonomy->filter_listing())
				continue;
				
            $type = $taxonomy->get_type();
            $labels = $taxonomy->get_labels();
            $select_option_all = __('All ' . $labels['name'], Plugin::TEXT_DOMAIN);
			
			wp_dropdown_categories(array(
				'show_option_all' =>  $select_option_all,
				'taxonomy'        =>  $type,
				'name'            =>  $type,
				'orderby'         =>  'name',
				'selected'        =>  isset($_GET[$type]) ? $_GET[$type] : null,
				'hierarchical'    =>  true,
				'depth'           =>  2,
				'show_count'      =>  false,
				'hide_empty'      =>  false,
			));
		}
    }

    public function filter_taxonomies($query) {
        global $pagenow;
		
		if(
            $pagenow !== 'edit.php' ||
            !isset($query->query_vars) ||
			!isset($query->query_vars['post_type']) || 
			$this->get_type() != $query->query_vars['post_type']
			
		)
			return;
		
		foreach($this->get_taxonomies() as $taxonomy) {
            $type = $taxonomy->get_type();
			if(isset($query->query_vars[$type]) && is_numeric($query->query_vars[$type])) {
				$query->query_vars[$type] = get_term_by(
                    'id', $query->query_vars[$type], $type
                )->slug;
			}
		}

    }
}