<?php
namespace balonka\quiz;

class FormSetup {
    protected $form_id;
    protected $post_id;
    protected $meta;

    public function __construct($post_id) {
        $this->post_id = $post_id;
        $this->meta = new PostMeta($post_id);
        $this->form_id = $this->meta->form_id;
    }

    public static function get_from_form_id($form_id) {
        $posts = get_posts([
            'numberposts'   => -1,
            'post_type'     => 'form-setup'
        ]);

        foreach($posts as $post) {
            $form_setup = new static($post->ID);
            if($form_setup->form_id == $form_id) {
                return $form_setup;
            }
        }
    }

    public function get_quiz_result($entry_id) {
        if($this->form_id) {
            $result = $this->get_quiz_entries_scores($this->form_id, $entry_id);
            $result['ending_msg'] = $this->get_quiz_ending_message($result['total_score']);

            return $result;
        }
    }

    protected function get_quiz_ending_message($total_score) {
        $survey_results = $this->meta->survey_results;
        if(!empty($survey_results['min_score'])) {
            foreach($survey_results['min_score'] as $i => $min_score) {
                $max_score = $survey_results['max_score'][$i];
                $message = $survey_results['message'][$i];

                if($min_score === '') {
                    $match = $max_score !== '' && $total_score <= $max_score;
                } elseif($max_score === '') {
                    $match = $min_score !== '' && $total_score >= $min_score;
                } else {
                    $match = $total_score >= $min_score && $total_score <= $max_score;
                }

                if($match) {
                    return $message;
                }
            }
        }
    }

    public function get_quiz_entries_scores($form_id, $entry_id) {
        $entry = get_gravity_form_entry($entry_id);
        $fields = get_gravity_form_fields($form_id, 'survey');
        $survey_scores = $this->meta->survey_scores;
        $survey_diragram_labels = $this->meta->survey_diagram_labels;

        $entry_scores = [
            'total_score'   => 0,
            'entries'       => []
        ];

        foreach($fields as $field) {
            if(isset($survey_scores[$field->id])) {
                foreach($field->choices as $choice) {
                    if(
                        isset($entry[$field->id])
                            && isset($survey_scores[$field->id][$choice['value']]) 
                            && $survey_scores[$field->id][$choice['value']] !== ''
                            && $entry[$field->id] == $choice['value']
                    ) {
                        $score = (float) $survey_scores[$field->id][$choice['value']];
                        $entry_scores['total_score'] += $score;
                        $entry_scores['entries'][] = [
                            'question'      => $field->label,
                            'score'         => $score,
                            'choice'        => $choice['text'],
                            'messages'      => $this->get_quiz_entry_conditional_messages($field->id, $choice),
                            'diagram_label' => isset($survey_diragram_labels[$field->id]) ? $survey_diragram_labels[$field->id] : $field->label
                        ];
                    }
                }
            }
        }

        return $entry_scores;
    }

    public function get_meta_survey_conditional_messages() {
        $survey_conditional_messages = $this->meta->survey_conditional_messages;
        $data = [];

        if(!empty($survey_conditional_messages)) {
            foreach($survey_conditional_messages as $field_id => $messaages) {
                $entry = [];

                foreach($messaages['match_any_all'] as $i => $match_any_all) {
                    $entry['match_any_all'] = $match_any_all;
                    $entry['message'] = $messaages['message'][$i];

                    foreach($messaages['choice_equals_not'][$i] as $j => $choice_equals_not) {
                        $entry['conditions'][$j]['choice_equals_not'] = $choice_equals_not;
                        $entry['conditions'][$j]['choice'] = $messaages['choice'][$i][$j];
                    }
                }

                $data[$field_id][] = $entry;
            }
        }

        return $data;
    }

    protected function get_quiz_entry_conditional_messages($field_id, $choice) {
        static $rules = [];
        $messaages  =[];

        if(empty($messaages)) {
            $rules = $this->get_meta_survey_conditional_messages();
        }

        if(isset($rules[$field_id])) {
            foreach($rules[$field_id] as $rule) {
                $rule_match_all = $rule['match_any_all'] == 'all';
                $match_all = !empty($rule['conditions']);

                foreach($rule['conditions'] as $condition) {
                    if($condition['choice_equals_not'] == 'equals') {
                        $match = $condition['choice'] == $choice['value'];
                    } else {
                        $match = $condition['choice'] != $choice['value'];
                    }

                    if($match && !$rule_match_all) {
                        $match_all = true;
                        break;
                    } else {
                        $match_all = $match_all && $match;
                        if(!$match_all) {
                            break;
                        }
                    }
                }

                if($match_all) {
                    $messaages[] = $rule['message'];
                }
            }
        }

        return $messaages;
    }

    public function get_quiz_result_page_url($entry_id, $query_args = []) {
        $result_page = $this->meta->survey_result_page;
        $option_key = sprintf(
            'result_token_%d_%d', $this->form_id, $entry_id
        );

        if(!($token = get_option($option_key))) {
            $token = uniqid();
            update_option($option_key, $token, false);
        }
        
        return add_query_arg(
            array_merge($query_args, [
                'entry_id'  => $entry_id,
                'form_id'   => $this->form_id,
                'token'     => $token
            ]),
            $result_page ? get_page_link($result_page) : site_url('/')
        );
    }

    public function validate_result_token($entry_id, $token) {
        $option_key = sprintf(
            'result_token_%d_%d', $this->form_id, $entry_id
        );

        return $token && $token === get_option($option_key);
    }

    public function __get($key) {
        switch($key) {
            case 'post_id':
            case 'form_id':
            case 'meta':
                return $this->$key;
        }
    }
}