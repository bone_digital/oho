<?php
namespace balonka\quiz;

abstract class Shortcode {
    abstract public function get_tag();
    abstract protected function output($atts, $content);

    public function __construct() {
        add_shortcode(
            $this->get_tag(), [$this, 'do_shortcode']
        );
    }

    public function do_shortcode($atts, $content) {
        ob_start();
        $this->output($atts, $content);

        return ob_get_clean();
    }
}