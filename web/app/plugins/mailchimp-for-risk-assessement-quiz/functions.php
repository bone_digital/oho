<?php
namespace balonka\quiz\mailchimp;

function load_template($name, $data = [], $options = []) {
    $options = array_merge([
        'echo'          => true,
        'only_once'     => false,
        'include_path'  => []
    ], $options);

    $trace = debug_backtrace(\DEBUG_BACKTRACE_IGNORE_ARGS | ~\DEBUG_BACKTRACE_PROVIDE_OBJECT, 1);
    $callee_dir = dirname($trace[0]['file']);

    $include_path = empty($options['include_path']) ? [] : (array) $options['include_path'];
    $include_path[] = $callee_dir;
    $include_path[] = $callee_dir . '/templates';

    preg_match('/\.php$/i', $name) or $name .= '.php';
    $exists = false;

    $load = function() {
        extract(func_get_arg(1));

        if(func_get_arg(2))
            include_once func_get_arg(0);
        else
            include func_get_arg(0);
    };

    foreach($include_path as $path) {
        $file = rtrim($path, '/') .'/'. $name;
        if(is_file($file)) {
            $exists = true;
            
            $options['echo'] or ob_start();
            $load($file, $data, $options['only_once']);
            if(!$options['echo']) {
                return ob_get_clean();
            }

            break;
        }
    }

    return $exists;
}

function send_json($success = true, $result = '') {
    if($success) {
        $json = [
            'success'   => true,
            'result'    => $result
        ];
    } else {
        $json = [
            'success'   => false,
            'error'     => $result
        ];
    }

    wp_send_json($json);
}

function subscribe($list_id, array $subscribers) {
    $params = [
        'update_existing'   => true,
        'members'           => $subscribers
    ];

    $log = logger('payload');
    $log('Dumping subscribe data: ');
    $log(print_r($params, true));
    
    $path = 'lists/' . $list_id;
    $log('Posting data to %s', $path);
    $response = post($path, $params, $info, $err);
    
    if($response === false)
        $log('Error subscribing: %s', print_r($err, true));
    else {
        $log('Subscriptions successfully completed, response: %s, info: %s', $response, $info);
        log('Subscriptions results:');
        log('New members: %d, updated members: %d, errors: %d', $response->total_created, $response->total_updated, $response->error_count);

        if($response->error_count > 0) {
            log('Getting %d error members...', $response->error_count);
            foreach($response->errors as $member) {
                log('Email: %s, error: %s, error_code: %s', $member->email_address, $member->error, $member->error_code);
            }
        }
    }
}

function unsubscribe($list_id, $emails) {
    $log = logger('payload');
    $log('Dumping unsubscribe emails: ');
    $log('%s', $emails);
    
    foreach($emails as $email) {
        update_subscriber($list_id, $email, [
            'status' => 'unsubscribed'
        ]);
    }
}

function update($list_id, $subscribers) {
    $log = logger('payload');
    $log('Dumping update subscribers: ');
    $log('%s', $subscribers);
    
    foreach($subscribers as $email => $subscriber) {
        update_subscriber($list_id, $email, $subscriber);
    }
}

function delete($list_id, $emails) {
    $log = logger('payload');
    $log('Dumping delete emails: ');
    $log('%s', $emails);
    
    foreach($emails as $email) {
        delete_subscriber($list_id, $email);
    }
}

function update_subscriber($list_id, $email, $data) {
    $log = logger('payload');

    $path = sprintf(
        'lists/%s/members/%s', 
        $list_id, md5(mb_strtolower($email))
    );

    $log('Posting data to %s', $path);
    $response = rest($path, 'patch', $data, $info, $err);
    $log('Response: %s', $response);

    if($response === false)
        log('Error updating subscriber email %s: %s', $email, $err);
    elseif(preg_match('/^4\d{2}$/', $response->status)) {
        if(isset($response->errors)) {
            foreach($response->errors as $error) {
                log('Error updating subscriber email %s field: %s, error: %s', $email, $error->field, $error->message);
            }
        }
    } else {
        log('Email %s successfully updated', $email);
    }
}

function delete_subscriber($list_id, $email) {
    $log = logger('payload');

    $path = sprintf(
        'lists/%s/members/%s/actions/delete-permanent', 
        $list_id, md5(mb_strtolower($email))
    );

    $log('Posting data to %s', $path);
    $response = rest($path, 'delete', $data, $info, $err);
    $log('Response: %s', $response);

    if($response === false)
        log('Error deleting subscriber email %s: %s', $email, $err);
    elseif(preg_match('/^4\d{2}$/', $response->status)) {
        log('Error deleting subscriber email %s: %s', $email, $response);
    } else {
        log('Email %s successfully deleted', $email);
    }
}

function get_all_emails($list_id, $id_col) {
    $max_count_per_page = 1000;
    $path = sprintf('lists/%s/members?count=%d', $list_id, $max_count_per_page);
    $members = [];
    $page = 1;

    $log = logger('payload');
    
    do {
        $log('Fetching subscribers from path: %s', $path);
        $result = get($path);

        if($result === false) {
            $log('Error fetching subscribers %s', $err);
            log('Unable to fetch subscribers, exiting now...');
            exit;
        }
        
        $log('%d subscribers fetched', count($result->members));

        $pages = ceil($result->total_items / $max_count_per_page);
        $offset = $page * $max_count_per_page;
        $path = sprintf(
            'lists/%s/members?count=%d&offset=%d', 
            $list_id, $max_count_per_page, $offset
        );

        if(isset($result->members)) {
            foreach($result->members as $member) {
                $id = $member->merge_fields->{$id_col};
                if(!empty($id)) {
                    $members[$id][$member->status][] = $member->email_address;
                }
                
            }
        } else {
            log('Unable to find members in the result object: %s', $result);
            log('exiting now...');
            exit;
        }
    } while($page++ < $pages);

    return $members;
}

function get_subscriber_fields($row, array $fields_map, &$email = null) {
    $fields = [];

    foreach($fields_map as $tag => $field) {
        $val = $row[$field];

        if($tag == 'EMAIL') {
            $email = $val;
        } else {
            $fields[$tag] = $val;
        }
    }

    return $fields;
}

function rest($path, $method = 'get', $params = [], &$info = array(), &$err = '') {
    $headers = [
        'Authorization' => 'Basic ' .base64_encode('apikey:' . API_KEY)
    ];

    $url = API_ENDPOINT .'/'. $path;
    
    switch(strtolower($method)) {
        case 'post':
        case 'put':
        case 'delete':
        case 'patch':
            $headers['Content-Type'] = 'application/json';
    }

    return curl($url, [
        'method'        => $method, 
        'params'        => $params, 
        'headers'       => $headers,
        'content_type'  => 'json'
    ], $info, $err);
}

function get($path, $params = [], &$info = array(), &$err = '') {
    return rest($path, 'get', $params, $info, $err);
}

function post($path, $params = [], &$info = array(), &$err = '') {
    return rest($path, 'post', $params, $info, $err);
}

function is_cli() {
	return !isset($_SERVER['SERVER_SOFTWARE']) && (
		php_sapi_name() == 'cli' || (is_numeric($_SERVER['argc']) && $_SERVER['argc'] > 0)
	);
}

function curl($url, $options = [], &$info = [], &$err = '') {
    static $ch;

    $defaults = [
        'method'            => 'get',
        'params'            => [],
        'headers'           => [],
        'content_type'      => '',
        'cookies_file'      => '',
        'timeout'           => 100,
        'connect_timeout'   => 8,
        'fail_on_http_error'=> false,
        'reinit'            => false,
        'ssl'               => [],
        'extra'             => []
    ];

    extract(array_merge($defaults, $options));
    
    $init = function() use(&$cookies_file, &$timeout, &$connect_timeout, $fail_on_http_error) {
        if( !function_exists('curl_init') ) {
			$err = "Curl Package not installed";
			throw new \Exception($err);
		}
		
		if( !$ch = curl_init() ) {
			$err = "Curl initialization failed for some unknown reasons";
			throw new \Exception($err);
        }
        
        /*
            As of PHP 4.4.4 FOLLOWLOCATION option has been disabled when
            - SAFE MODE has been tunred on
            - OPEN_BASEDIR has been set
        */
        if(!ini_get('safe_mode') && !ini_get('open_basedir')) {
            // Redirection will be handled automatically        
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }
        
        //curl_setopt($ch, CURLOPT_VERBOSE, true);

        if(!empty($cookies_file)) {
            // file to save the cookies
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookies_file);
            
            // file to load the cookies
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookies_file);
        }
        
        // set response to be returned as a string, defaults to output directly
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        // include http response headers in the output	
        curl_setopt($ch, CURLOPT_HEADER, true);
        
        // sets the Referer http header automatically during redirection	
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    
        curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $connect_timeout );
        
        curl_setopt( $ch, CURLOPT_FAILONERROR, $fail_on_http_error );

        return $ch;
    };

    $set_options = function() use(&$ch, &$method, &$params, &$headers, &$url, &$ssl, &$extra) {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));

        switch(strtolower($method)) {
            case 'head':
                curl_setopt($ch, CURLOPT_NOBODY, true);
            case 'get':
                $query = [];
                foreach($params as $key => $value)
                    $query[] = $key .'='. urlencode($value);
    
                if(!empty($query))
                    $url .= (strpos($url, '?') === false ? '?' : '&') . implode('&', $query);
                break;
    
            case 'post':
            case 'put':
            case 'delete':
            case 'patch':
                if(is_array($params) && isset($headers['Content-Type'])) {
                    list($ct, $encoding) = preg_split('/\s*;\s*/', $headers['Content-Type'], 2);
                    if($ct == 'application/json')
                        $params = json_encode($params);
                }
    
                empty($params) or curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
    
        $req_headers = [];
        foreach($headers as $key => $value)
            $req_headers[] = "$key: $value";
    
        if(!empty($req_headers))
            curl_setopt ($ch, CURLOPT_HTTPHEADER, $req_headers);
    
        // set url
        curl_setopt($ch, CURLOPT_URL, encode_url($url));
    
        // handle https
        if(strpos(strtolower($url), 'https://') === 0) {
            if(empty($ssl)) {
                $ssl = [
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => 2
                ];
            }
            
            curl_setopt_array($ch, $ssl);
        }

        empty($extra) or curl_setopt_array($ch, $extra);
    };

    $execute = function(&$resHeaders = array(), &$statusCode = 0, &$statusMsg = '') use(&$ch, &$execute) {
        $args = func_get_args();
		static $followRedirectionCount = 0;
		static $maxRedirection = 30;
		
		if(isset($args[4]) && $args[4] === $followRedirectionCount)
			$followRedirectionCount++;
		else
			$followRedirectionCount = 0;
		
		if($followRedirectionCount >= $maxRedirection) {
			$followRedirectionCount = 0;
			return false;
		}
		
		// execute curl and get the response headers and body
		$response = curl_exec($ch);
		if($response === false)
			return false;
		
		// curl follow location not set if safe mode and open_basedir is turned on
		$curlFollowLocationDisabled = ini_get('safe_mode') || ini_get('open_basedir');
		
		// curl includes all the previous redirection headers as well in the final response
		// strip all the previous redirection headers from the final response.
		do {
			list($responseHeaders, $responseBody) = explode("\r\n\r\n", $response, 2);
			$responseHeaderLines = explode("\r\n", $responseHeaders);
				   
			// http status line
            $httpResponseLine = array_shift($responseHeaderLines);
            
			// HTTP/1.1 200 OK
			if( preg_match('@^HTTP/[0-9]\.[0-9] ([0-9]{3})( ([a-zA-Z]+))?@',$httpResponseLine, $matches) ) {
				$statusCode = $matches[1] ;
				$statusMsg = $matches[2] ;
			}
			
			$responseWasRedirection = ($statusCode >= 300 && $statusCode < 400) || $statusCode == 100;
			$resHeaders = array();
			foreach( $responseHeaderLines as $headerLine ) {
				list($header, $value) = explode(':', $headerLine, 2);
				$resHeaders[trim($header)] = trim($value) ;
			}
			
			if($responseWasRedirection) {
				list(, $response) = explode("\r\n\r\n", $response, 2);
				if(empty($response)) break;
			}
			
		} while(!$curlFollowLocationDisabled && $responseWasRedirection);
		
		// handle redirection from the script, curl wont be handling this
		if($curlFollowLocationDisabled && $responseWasRedirection){
			if(isset($resHeaders['Location'])) {
				$lastUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
				$redirectUrl = to_absolute_url($resHeaders['Location'], $lastUrl);
				curl_setopt ($ch, CURLOPT_HTTPHEADER, array("Referer: $lastUrl"));
				curl_setopt($ch, CURLOPT_URL, $redirectUrl);
				
				return $execute($resHeaders, $statusCode, $statusMsg, $followRedirectionCount);
			}
		}
		
		return $responseBody;
    };

    if($reinit || !$ch) {
        $ch = $init($err);
    }
    
    $set_options();
	$body = $execute($resHeaders, $statusCode, $statusMsg) ;
	$info = array(
		'status_code' => $statusCode, 
		'status_msg' => $statusMsg, 
		'headers' => $resHeaders,
		'last_url' => curl_getinfo($ch, CURLINFO_EFFECTIVE_URL)
	);
	    
    if($body === false) {
        $err = curl_error($ch);
        //log('Error fetching url %s error: %s info: %s', $url, $err, print_r($info, true));
    } else {
        $is_json = $content_type == 'json';
        if(!$is_json && isset($resHeaders['Content-Type'])) {
            list($ct, $encoding) = preg_split('/\s*;\s*/', $resHeaders['Content-Type'], 2);
            $is_json = $ct == 'application/json';
        }

        !$is_json or $body = json_decode($body);
    }

    return $body;
}

/*
    Does not parse url based on RFC 1808, instead parses url 
    based on coders parctice on resolving realtive to absolute url.
    Should be based on RFC 1808.
*/
function to_absolute_url($relativeUrl, $refUrl){
    // remove querystring form the url
    $refUrl = ($qPos = strpos($refUrl, '?')) !== false ? substr($refUrl, 0, $qPos) : $refUrl;
        
    $urlParts = parse_url($refUrl);
    $url = "{$urlParts['scheme']}://{$urlParts['host']}";
    $path = $urlParts['path'];
        
    $relativeUrlParts = parse_url($relativeUrl);
    if(preg_match('@^http(s)?://[^/]+@', $relativeUrl)) return $relativeUrl;
    
    if( strpos($relativeUrl, '..') !== 0 ) {
        if( strpos($relativeUrl, '/') !== 0 ) {
            $newPath = strpos( $relativeUrl, './' ) === 0 ? substr( $relativeUrl, 2 ) : $relativeUrl;
            if( strrpos($path, '/') == strlen($path) - 1 ){
                // append the $relativeUrl
                $absUrl = $refUrl.$newPath;
            } else {
                $dirname = dirname($path);
                $path1 =  $dirname == '/' || $dirname == '\\' ? $newPath : "$dirname/$newPath" ;
                $absUrl = $url.(strpos($path1,'/') === 0 ? $path1 : "/$path1");
            }
            return $absUrl;
        }
        else { // absolute path
            return ( $url.$relativeUrl );
        }
    } else {
        $path = strrpos($path, '/') == strlen($path) - 1 ? $path : dirname($path);
        if( $path != '/' || $path != '\\' ) {
            $path = strrpos($path, '/') == strlen($path) - 1 ? substr($path, 1, strlen($path) - 2) : substr($path, 1);
            $paths = explode('/', $path);
            $relPaths = explode('/', $relativeUrl);
            foreach( $relPaths as $item ) {
                if( $item == '..' ) array_pop($paths);
                else {
                    $newPath .= "/$item";
                }
            }
            $absUrl = (count($paths) > 0 ? "$url/".join('/', $paths) : $url) . $newPath;
        }
        return $absUrl;
    }
}

function encode_url($url) {
    list($u, $q) = explode('?', $url, 2);
    $query = [];

    if(preg_match_all('/([^=&]+) (?:= ([^&]+))?/x', $q, $matches)) {
        foreach($matches[1] as $i => $key) {
            $key = url_encode($key);

            if(strpos($matches[0][$i], '=') === false)
                $query[] = $key;
            else
                $query[] = "$key=" . url_encode($matches[2][$i]);
        }

        $url = $u .'?'. implode('&', $query);
    }

    return $url;
}

function url_encode($string) {
    return urlencode(urldecode($string));
}


function log($config = []) {
    if(is_array($config))
        return logger('global', $config);

    return call_user_func_array(
        logger('global'), func_get_args()
    );
}

function logger($id = 'global', array $config = []) {
    static $logs = [], $last_id, $shutdown_registered = false;

    if(empty($id)) {
        $id = isset($last_id) ? $last_id : 'global';
    }

    $defaults = [
        'mode'      => 'a',
        'max_logs'  => 3,
        'header'    => "$id logs",
        'footer'    => 'End of log',
        'save_path' => defined('LOGS_PATH') ? LOGS_PATH : __DIR__ . '/logs',
        'log_to'    => 'file'
    ];

    $log = function($config) use($id) {
        return function($line) use($id, &$config) {
            static $fp;

            $now = date('Y-m-d H:i:s');
            $today = date('Y-m-d');
            $lines = [];

            $log_to = function($key) use(&$config) {
                return in_array(
                    $key, preg_split('/\s+/', $config['log_to'])
                );
            };

            $out = function($lines) use(&$fp, &$log_to) {
                if(is_array($lines)) {
                    $lines = implode("\n", $lines);
                }

                $lines .= "\n";

                if($log_to('file')) {
                    fwrite($fp, $lines, strlen($lines));
                }
                
                if($log_to('stdout')) {
                    if(is_cli()) {
                        echo $lines;
                    } else {
                        echo "<pre>$lines</pre>";
                        ob_flush();
                        flush();
                    }
                }
            };

            if(!$fp) {
                if($log_to('file')) {
                    $save_path = $config['save_path'] . "/$id";
                    is_dir($save_path) 
                        or mkdir($save_path, 0755, true);

                    $file = sprintf('%s/%s.log', $save_path, $today);
                    $fp = fopen($file, $config['mode']);

                    if(!$fp) {
                        return false;
                    }
                } else {
                    $fp = true;
                }
                
                $lines[] = '# ' . $config['header'];
                $lines[] = '#------------------------------------------------------------------';
            }
            
            if(strtoupper($line) == '%EOL%') {
                $lines[] = "[$now] " . $config['footer'];
                $lines[] = '#------------------------------------------------------------------';
                $out($lines);

                if($log_to('file')) {
                    fclose($fp);
                    unset($fp);
                }
            } else {
                $args = [];
                for($i = 1; $i < func_num_args(); $i++) {
                    $arg = func_get_arg($i);
                    switch(true) {
                        case is_bool($arg):
                            $arg = $arg ? 'true' : 'false';
                            break;
                            
                        case is_array($arg):
                        case is_object($arg):
                            $arg = print_r($arg, true);
                            break;
                            
                        case is_resource($arg):
                            $arg = 'resource#' . get_resource_type($arg);
                    }
                    
                    $args[] = $arg;
                }

                empty($args) or $line = vsprintf($line, $args);
                $line = (preg_match('/^\s*$/', $line) ? '' : "[$now] ") . $line;
                $lines[] = $line;
                $out($lines);
            }
        };
    };

    if(!isset($logs[$id]) || !empty($config)) {
        $config = array_merge($defaults, $config);
        $logs[$id]['config'] = $config;
        $logs[$id]['object'] = $log($config);
    }
    
    $last_id = $id;

    if(!$shutdown_registered) {
        register_shutdown_function(function() use(&$logs) {
            foreach($logs as $id => $data) {
                $config = $data['config'];
                $save_path = $config['save_path'] . "/$id";
                if(!is_dir($save_path)) continue;
                
                $dir = dir($save_path);
                $files = [];

                while(false != ($entry = $dir->read())) {
                    if(!preg_match('/^(\d{4}-\d{2}-\d{2})\.log$/', $entry, $matches))
                        continue;

                    list(, $date) = $matches;
                    $files[strtotime($date)] = $save_path .'/'. $entry;
                }

                $dir->close();

                if(count($files) > $config['max_logs']) {
                    krsort($files);
                    array_splice($files, 0, $config['max_logs']);

                    foreach($files as $file) {
                        unlink($file);
                    }
                }
            }
        });

        $shutdown_registered = true;
    }

    return $logs[$id]['object'];
}


function mb_strcasecmp($str1, $str2, $encoding = null) {
    if (null === $encoding) { $encoding = mb_internal_encoding(); }
    return strcmp(mb_strtoupper($str1, $encoding), mb_strtoupper($str2, $encoding));
}