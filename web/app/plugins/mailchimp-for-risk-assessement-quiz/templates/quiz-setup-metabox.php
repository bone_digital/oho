<div class="mc4quiz-setup" data-post-id="<?= $post_id ?>" data-form-id="<?= $form_id ?>">
    <div class="fields-row">
        <div class="field-label">
            <label><?php _e('Enable MailChimp Setup', $textdomain) ?></label>
        </div>
        <div class="field-input">
            <label>
                <input type="checkbox" name="enable_setup" value="1" <?php checked(1, isset($mc4quiz['enable_setup']) ? $mc4quiz['enable_setup'] : 0) ?>>
                <?php _e('Yes', $textdomain) ?>
            </label>
        </div>
    </div>

    <div class="fields-row">
        <div class="field-label">
            <label for="mc4quiz_connection_id"><?php _e('Select a connection', $textdomain) ?></label>
        </div>
        <div class="field-input">
            <select name="connection_id" id="mc4quiz_connection_id">
                <option value><?php _e('-Select connection-', $textdomain) ?></option>
                <?php foreach($connections as $connection) : ?>
                    <option value="<?= $connection['id'] ?>" <?php selected(true, $connection['selected']) ?>>
                        <?= $connection['name'] ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
    </div>

    <?= $mappings_fields ?>
</div>