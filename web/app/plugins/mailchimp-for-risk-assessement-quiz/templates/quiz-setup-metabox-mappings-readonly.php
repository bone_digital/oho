<div class="fields-row mappings-row mapping-readonly-row">
    <div class="field-label">
        <label><?php _e('Mappings', $textdomain) ?></label>
    </div>
    <div class="field-input">
        <div class="mappings">
            <div class="field-header"><?php _e('MailChimp Merge Field') ?></div>
            <div class="field-header"><?php _e('Gravity Form / Quiz Field') ?></div>
            <?php for($i=0; $i<count($merge_fields); $i++) : ?>
                <div class="merge-field">
                    <?= $merge_fields[$i] ?>
                </div>

                <div class="form-field">
                    <?= $form_fields[$i] ?>
                </div>
            <?php endfor ?>
        </div>
        <div class="remap">
            <button class="button" type="button" name="remap"><?php _e('ReMap', $textdomain) ?></button>
        </div>
    </div>
</div>
