<div class="fields-row">
    <div class="field-label">
        <label for="form_id"><?php _e('Select a list / audience to connect to', $textdomain) ?></label>
    </div>
    <div class="field-input">
        <select name="list">
            <?php foreach($lists as $list) : ?>
                <option value="<?= $list->id .':'. $list->name ?>">
                    <?= $list->name ?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
</div>