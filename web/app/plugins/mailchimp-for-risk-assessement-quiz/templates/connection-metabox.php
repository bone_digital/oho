<div class="mc4quiz mc4quiz-metabox-container" id="mc4quiz-connection-metabox" data-post-id="<?= $post_id ?>">
    <?php if($apikey) : ?>
        <div class="fields-row">
            <div class="field-label">
                <label for="form_id"><?php _e('API Key', $textdomain) ?></label>
            </div>
            <div class="field-input apikey">
                <?= $apikey ?>
                <button type="button" class="button" name="reset"><?php _e('Reset', $textdomain) ?></button>
            </div>
        </div>
        <div class="fields-row">
            <div class="field-label">
                <label for="form_id"><?php _e('List / Audience', $textdomain) ?></label>
            </div>
            <div class="field-input apikey">
                <?= $list_name ?>
            </div>
        </div>
    <?php else : ?>
        <div class="fields-row">
            <div class="field-label">
                <label for="form_id"><?php _e('API Key', $textdomain) ?></label>
            </div>
            <div class="field-input apikey">
                <input type="text" name="apikey" value="" class="regular-text">
                <button type="button" class="button" name="connect" disabled><?php _e('Connect', $textdomain) ?></button>
            </div>
        </div>
    <?php endif ?>
</div>