<div class="fields-row mappings-row">
    <div class="field-label">
        <label><?php _e('Mappings', $textdomain) ?></label>
    </div>
    <div class="field-input">
        <div class="mappings">
            <div class="field-header"><?php _e('MailChimp Merge Field') ?></div>
            <div class="field-header"><?php _e('Gravity Form / Quiz Field') ?></div>
            <?php for($i=0; $i<count($merge_fields) + 1; $i++) : ?>
                <div class="merge-field">
                    <select name="merge_fields[]">
                        <option value><?php _e('- Select field -', $textdomain) ?></option>
                        <optgroup label="<?php _e('Primary field', $textdomain) ?>">
                            <option value="email:<?php _e('Email', $textdomain) ?>" <?php selected('email', empty($saved) ? '' : $saved['merge_fields'][$i]) ?>>
                                <?php _e('Email', $textdomain) ?>
                            </option>
                        </optgroup>
                        <optgroup label="<?php _e('Merge fields', $textdomain) ?>">
                            <?php foreach($merge_fields as $field) : ?>
                                <option value="<?= $field->tag .':'. $field->name ?>" <?php selected($field->tag, empty($saved) ? '' : $saved['merge_fields'][$i]) ?>>
                                    <?= $field->name ?>
                                </option>
                            <?php endforeach ?>
                        </optgroup>
                    </select>
                </div>

                <div class="form-field">
                    <select name="form_fields[]">
                        <option value><?php _e('- Select field -', $textdomain) ?></option>
                        <optgroup label="<?php _e('Gravity form fields', $textdomain) ?>">
                            <?php foreach($form_fields as $field) : ?>
                                <?php if(!empty($field->inputs)) : ?>
                                    <?php foreach($field->inputs as $input) : ?>
                                        <?php if(!$input['isHidden']) : ?>
                                            <option value="<?= $input['id'] ?>" <?php selected($input['id'], empty($saved) ? '' : $saved['form_fields'][$i]) ?>>
                                                <?= $field->label .'('.$input['label'].')' ?>
                                            </option>            
                                        <?php endif ?>
                                    <?php endforeach ?>
                                <?php else : ?>
                                    <option value="<?= $field->id ?>" <?php selected($field->id, empty($saved) ? '' : $saved['form_fields'][$i]) ?>>
                                        <?= $field->label ?> (<?= $field->type ?>)
                                    </option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </optgroup>
                        <optgroup label="<?php _e('Quiz fields', $textdomain) ?>">
                            <option value="quiz_score" <?php selected('quiz_score', empty($saved) ? '' : $saved['form_fields'][$i]) ?>>
                                <?php _e('Quiz Score', $textdomain) ?>
                            </option>
                            <option value="quiz_result_page_url" <?php selected('quiz_result_page_url', empty($saved) ? '' : $saved['form_fields'][$i]) ?>>
                                <?php _e('Quiz Result Page URL', $textdomain) ?>
                            </option>
                        </optgroup>
                    </select>
                </div>
            <?php endfor ?>
        </div>
    </div>
</div>
