<?php
namespace balonka\quiz\mailchimp;

class Client {
    protected $api_key;
    protected $request;

    public function __construct($api_key) {
        list(, $dc) = explode('-', $api_key, 2);
        $api_base = "https://$dc.api.mailchimp.com/3.0/";

        $this->api_key = $api_key;
        $this->request = new Request($api_base);
        $this->request->set_content_type('json');
        $this->request->set_authorization(
            'Basic ' . base64_encode('apikey:' . $api_key)
        );
    }

    public function ping() {
        return $this->request->send('GET', 'ping');
    }

    public function get_lists() {
        $lists = [];
        $this->walk_multipage_resources('lists', function($result) use(&$lists) {
            $lists = array_merge($lists, $result->lists);
        });

        return $lists;
    }

    public function get_list_merge_fields($list_id) {
        $response = $this->request->send('GET', "lists/$list_id/merge-fields");
        $fields = [];

        foreach($response->merge_fields as $field) {
            $fields[$field->merge_id] = $field;
        }

        return $fields;
    }

    public function subscribe($list_id, array $subscribers, $update_existing = true) {
        $params = [
            'update_existing'   => $update_existing,
            'members'           => $subscribers
        ];
    
        $log = Logger::get(__FUNCTION__);
        $log('Dumping subscribe data: ');
        $log('%s', $params);
        
        $path = 'lists/' . $list_id;
        $log('Posting data to %s', $path);
        $response = $this->request->send('POST', $path, $params);
        
        $log('Subscriptions successfully completed, response: %s, info: %s', $response, $info);
        log('Subscriptions results:');
        log('New members: %d, updated members: %d, errors: %d', $response->total_created, $response->total_updated, $response->error_count);

        if($response->error_count > 0) {
            log('Getting %d error members...', $response->error_count);
            foreach($response->errors as $member) {
                log('Email: %s, error: %s, error_code: %s', $member->email_address, $member->error, $member->error_code);
            }
        }
    }

    public function walk_list_emails($list_id, callable $callback) {
        $max_count_per_page = 1000;
        $path = sprintf('lists/%s/members?count=%d', $list_id, $max_count_per_page);
        $page = 1;
    
        $log = Logger::get(__FUNCTION__);
        
        do {
            $log('Fetching subscribers from path: %s', $path);
            $result = $this->request->send('GET', $path);
                
            if($result === false) {
                $log('Error fetching subscribers %s', $err);
                log('Unable to fetch subscribers, returning now...');
                return false;
            }
            
            $log('%d subscribers fetched', count($result->members));
    
            $pages = ceil($result->total_items / $max_count_per_page);
            $offset = $page * $max_count_per_page;
            $path = sprintf(
                'lists/%s/members?count=%d&offset=%d', 
                $list_id, $max_count_per_page, $offset
            );
    
            if(isset($result->members)) {
                foreach($result->members as $member) {
                    if(call_user_func($callback, $member, $result->total_items) === false) {
                        return;
                    }
                }
            } else {
                $log('Unable to find members in the result object: %s', $result);
                $log('returning now...');
                return false;
            }
        } while($page++ < $pages);
    
        return true;
    }

    protected function walk_multipage_resources($o_url, callable $callback, $max_count_per_page = 1000) {
        $url = sprintf('%s?count=%d', $o_url, $max_count_per_page);
        $page = 1;
    
        $log = Logger::get(__FUNCTION__);
        
        do {
            $log('Fetching multi-page resource from url: %s', $url);
            $result = $this->request->send('GET', $url);
            $log('Multi-page fetch result: %s', $result);
                
            call_user_func($callback, $result);

            $pages = ceil($result->total_items / $max_count_per_page);
            $offset = $page * $max_count_per_page;
            $url = sprintf('%s?count=%d&offset=%d', $o_url, $max_count_per_page, $offset);
    
        } while($page++ < $pages);
    
        return true;
    }
}