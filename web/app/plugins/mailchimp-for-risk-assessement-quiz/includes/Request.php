<?php
namespace balonka\quiz\mailchimp;

use Exception;
use WP_Error;

class Request {
    public $base_url;
    protected $authorization;
    protected $content_type;
    protected $headers = [];

    public function __construct($base_url = '') {
        $this->base_url = $base_url;
    }

    public function set_authorization($authorization) {
        $this->authorization = $authorization;
    }

    public function set_content_type($type, $charset = 'utf-8') {
        switch($type) {
            case 'json':
                $type = 'application/json';
                break;

            case 'xml':
                $type = 'application/xml';
                break;
        }

        $this->content_type = [$type];
        empty($charset) or $this->content_type[] = "charset=$charset";
    }

    public function add_header($name, $value) {
        $this->headers[$name] = $value;
    }

    public function send($method = 'GET', $url, array $data = [], $options = []) {
        $log = Logger::get('request');

        $options = array_merge(
            [
                'return'    => 'body',
                'callback'  => null
            ], 
            $options
        );

        $uri = rtrim($this->base_url, '/') .'/'. ltrim($url, '/');
        
        $args = $this->get_args($method, $data);
    
        $log('Sending %s request to %s with args %s', strtoupper($method), $uri, $args);

        $result = wp_remote_request($uri, $args);

        if($result instanceof WP_Error) {
            $log(
                'Error sending request (%s) %s', $result->get_error_code(), $result->get_error_message()
            );

            throw new Exception('Error sending request');
        }

        $log("Received response:\n%s", $result['http_response']->get_response_object()->raw);

        if(is_callable($options['callback'])) {
            $result = call_user_func($options['callback'], $result);
        }

        if($options['return'] == 'body') {
            $body = $result['body'];
            $response_headers = $result['http_response']->get_headers();
            list($type, $charset) = preg_split('/\s*;\s*/', $response_headers['Content-Type'], 2);

            switch(strtolower($type)) {
                case 'application/json':
                case 'application/problem+json':
                    $body = json_decode($body);    
            }
            
            return $body;
        }

        return $result;
    }

    protected function get_args($method = 'GET', array $data = []) {
        $args = [
            'method' => strtoupper($method),
            'timeout'=> 20
        ];

        $headers = $this->headers;

        if(!empty($this->authorization)) {
            $headers['Authorization'] = $this->authorization;
        }

        if(!empty($this->content_type)) {
            $headers['Content-Type'] = implode('; ', $this->content_type);
        }

        if(!empty($data)) {
            $content_type = isset($this->content_type[0]) ? strtolower($this->content_type[0]) : '';
            switch($content_type) {
                case 'application/json':
                    $args['body'] = json_encode($data);
                    $headers['Accept'] = 'application/json';
                    break;

                case 'text/xml':
                    $headers['Accept'] = 'text/xml';
            }

            isset($args['body']) or $args['body'] = $data;
            $args['data_format'] = 'body';
        }

        $args['headers'] = $headers;

        return $args;
    }
}
