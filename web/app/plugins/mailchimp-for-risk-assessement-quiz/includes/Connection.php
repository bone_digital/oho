<?php
namespace balonka\quiz\mailchimp;

use balonka\quiz\PostMeta;

class Connection {
    protected $post_id;
    protected $meta;

    public function __construct($post_id) {
        $this->post_id = $post_id;
        $this->meta = new PostMeta($post_id);
    }

    public function get_api_key() {
        return $this->meta->apikey;
    }

    public function get_list_id() {
        $list = $this->meta->list;
        if(!empty($list)) {
            list($list_id, $list_name) = explode(':', $list, 2);
            return $list_id;
        }
    }

    public function get_list_name() {
        $list = $this->meta->list;
        if(!empty($list)) {
            list($list_id, $list_name) = explode(':', $list, 2);
            return $list_name;
        }
    }

    public function __get($key) {
        switch($key) {
            case 'post_id':
            case 'meta':
                return $this->$key;
        }
    }

    public function set_api_key($apikey) {
        $this->meta->apikey = $apikey;
    }

    public function set_list($list) {
        if(func_num_args() > 1) {
            $list = $list .':'. func_get_arg(1);
        }

        $this->meta->list = $list;
    }

    public function reset() {
        $this->meta->delete('apikey', 'list');
    }
}