<?php
/*
Plugin Name: Mailchimp for Risk Assessment Quiz 2.0
Plugin URI:  https://balonka.se
Version:     1.0
Author:      Balonka AB
Author URI:  https://balonka.se
Text Domain: balonka-mc4quiz
Domain Path: /languages
*/


namespace balonka\quiz\mailchimp;

use balonka\quiz\Loader;
use balonka\quiz\PostType;
use Exception;
use balonka\quiz\Plugin as QuizPlugin;
use balonka\quiz\FormSetup;
use function balonka\quiz\get_gravity_form_fields;

class Plugin {
    const TEXT_DOMAIN = 'balonka-mc4quiz';

    private static $instance;

	public $url;
    public $dir;
    private $options_ns = 'balonka-mc4quiz_options';
    private $settings = [];
    private $template;
    private $forms = [];
    private $gateways = [];
    

    private function __construct() {ini_set('display_errors', true);
        $this->url = plugins_url('', __FILE__);
        $this->dir = __DIR__;
        $this->settings = get_option($this->options_ns, []);

        require_once __DIR__ . '/lib/vendor/autoload.php';
        require_once __DIR__ . '/defines.php';
        require_once __DIR__ . '/functions.php';
        
        Loader::mapNamespace(__NAMESPACE__, __DIR__ . '/includes');

        Logger::set_defaults([
            'save_path' => __DIR__ . '/logs'
        ]);

        $this->register_types();
        
        add_action('admin_menu', [$this, 'register_admin_menu']);
        add_action('after_quiz_form_submission', [$this, 'after_quiz_form_submission'], 10,  3);
        add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_scripts']);
        add_action('wp_ajax_mc4quiz/connect', [$this, 'ajax_connect']);
        add_action('wp_ajax_mc4quiz/reset', [$this, 'ajax_reset']);
        add_action('wp_ajax_mc4quiz/load_mappings', [$this, 'ajax_load_mappings']);
        add_action('wp_ajax_mc4quiz/remap', [$this, 'ajax_remap']);
    }

    private function register_types() {
        $connection = new PostType('mc4quiz-connection', 'Connection');
        $connection->set_parent_menu('balonka-mc4quiz');

        $connection->set_args([
            'public'             	=> false,
            'publicly_queryable' 	=> false,
            'show_ui'            	=> true,
            'show_in_menu'       	=> true,
            'exclude_from_search'   => true,
            'supports'              => ['title']
        ]);

        $connection->add_meta_box('mc4quiz-connection', [
            'title' => 'MailChimp Connection',
            'context' => 'normal',
            'priority' => 'high',
            'render' => [$this, 'render_connection_metabox'],
            'save' => function($post_id) {
                $connection = new Connection($post_id);
                if(!empty($_POST['apikey'])) {
                    $connection->set_api_key($_POST['apikey']);
                    $connection->set_list($_POST['list']);
                }
            }
        ]);

        QuizPlugin::getInstance()->get_post_type('form-setup')->add_meta_box(
            'mc4quiz-setup', [
                'title' => 'MailChimp Setup',
                'context' => 'normal',
                'priority' => 'high',
                'render' => [$this, 'render_quiz_setup_metabox'],
                'save' => function($post_id) {
                    $form_setup = new FormSetup($post_id);
                    $saved = $form_setup->meta->mc4quiz;
                    $saved = array_merge(
                        (array) $saved, (array) $_POST['mc4quiz']
                    );
                    
                    $form_setup->meta->update('mc4quiz', $saved);
                }
            ]
        );
        
    }

    public function admin_enqueue_scripts() {
        $screen = get_current_screen();
        $load_ids = ['mc4quiz-connection', 'form-setup'];

        foreach($load_ids as $id) {
            if(strstr($screen->id, $id)) {
                wp_enqueue_style('mc4quiz-admin-style', $this->url . '/assets/mc4quiz.admin.css');
                wp_enqueue_script('mc4quiz-admin-script', $this->url . '/assets/mc4quiz.admin.js', ['jquery'], false, true);
                wp_localize_script('mc4quiz-admin-script', 'mc4quiz_config', [
                    'ajax_url'      => admin_url( 'admin-ajax.php' ),
                    'save_settings' => 'save_settings'
                ]);

                break;
            }
        }
    }

    public function register_admin_menu() {
        add_menu_page(
            __('Mailchimp for Risk Assessment Quiz 2.0', self::TEXT_DOMAIN),
            __('Mailchimp for Risk Assessment Quiz 2.0', self::TEXT_DOMAIN),
            'manage_options',
            'balonka-mc4quiz',
            [$this, 'render_page']
        );

        /* add_submenu_page(
			'donation',
			__('Donation Settings', self::TEXT_DOMAIN),
			__('Settings', self::TEXT_DOMAIN),
			'manage_options',
			'donation/settings',
			[$this, 'render_page']
        ); */
    }

    public function render_page() {
        $textdomain = self::TEXT_DOMAIN;

        switch($_GET['page']) {
            default:
                $settings = $this->settings;

                $selected_tab = 'settings';
                ob_start();
                include 'templates/settings-fields.php';

                $fields = $this->apply_field_ns(
                    ob_get_clean(),
                    $this->options_ns
                );
        }

                
        include __DIR__ . '/templates/main.php';
    }

    public function render_connection_metabox($post) {
        $textdomain = self::TEXT_DOMAIN;
        
        $connection = new Connection($post->ID);
        load_template('connection-metabox', [
            'apikey'        => $connection->get_api_key(),
            'list_id'       => $connection->get_list_id(),
            'list_name'     => $connection->get_list_name(),
            'post_id'       => $post->ID,
            'textdomain'    => $textdomain
        ]);
    }

    public function render_quiz_setup_metabox($post) {
        $form_setup = new FormSetup($post->ID);
        $saved = $form_setup->meta->mc4quiz;
        $connections = [];
        $posts = get_posts([
            'post_type'     => 'mc4quiz-connection',
            'numberposts'   => -1,
        ]);

        foreach($posts as $p) {
            $connections[] = [
                'id'        => $p->ID,
                'name'      => $p->post_title,
                'selected'  => !empty($saved['connection_id']) ? $saved['connection_id'] == $p->ID : false
            ];
        }

        $mappings_fields = '';
        if(!empty($saved) && $saved['connection_id'] && $form_setup->form_id) {
            $mappings_fields = $this->load_mappings(
                $saved['connection_id'], $form_setup->form_id, $saved, true
            );
        }

        echo $this->apply_field_ns(
            load_template('quiz-setup-metabox', [
                'post_id'           => $post->ID,
                'textdomain'        => $textdomain,
                'connections'       => $connections,
                'form_id'           => $form_setup->form_id,
                'mc4quiz'           => $saved,
                'mappings_fields'   => $mappings_fields
            ],
            [
                'echo'              => false
            ]
        ),
            'mc4quiz'
        );
    }

    public static function getInstance() {
        if(empty(self::$instance)) {
            self::$instance = new static;
        }

        return static::$instance;
    }

    public function admin_init() {
        
    }

    
    private function apply_field_ns($input, $ns) {
		return preg_replace_callback(
			'/(<(?: input | select | textarea) \s+ [^>]* (?<=\s) name \s*=\s* ["\']) ( (?(?<=") [^"]+ | [^\']+) )/isx',
			function($matches) use($ns) {
				return $matches[1] . preg_replace(
					'/^[^\[\]]+/', $ns.'[$0]', $matches[2]
				);
			},
			$input
		);
    }
    
    
    public function ajax_save_settings() {
        $data = [];
        if(!empty($_POST['data'])) {
            parse_str($_POST['data'], $data);
        } else {
            $data = $_POST;
        }

        $status = ['success' => false];

        if(isset($data[$this->options_ns])) {
            $status['success'] = update_option($this->options_ns, $data[$this->options_ns], false);
        }
        
        echo json_encode($status);
        wp_die();
    }

    public function ajax_connect() {
        if(!empty($_POST['apikey'])) {
            try {
                $client = new Client($_POST['apikey']);
                $lists = $client->get_lists();
                
                $data['success'] = true;
                $data['result'] = load_template(
                    'connection-metabox-lists', ['lists' => $lists], [
                        'echo' => false
                    ]
                );
            } catch(Exception $e) {
                $data['success'] = false;
                $data['error'] = $e->getMessage();
            }

            wp_send_json($data);
        }
    }

    public function ajax_load_mappings() {
        if(!empty($_POST['connection_id']) && !empty($_POST['form_id'])) {
            $result = $this->load_mappings(
                $_POST['connection_id'], $_POST['form_id']
            );
        } else {
            $result = '';
        }

        wp_send_json([
            'success'   => true,
            'result'    => [
                '.mappings-row' => $result
            ]
        ]);
    }

    protected function load_mappings($connection_id, $form_id, $saved = [], $readonly = false) {
        $textdomain = self::TEXT_DOMAIN;

        if(!$readonly) {
            $connection = new Connection($connection_id);
                
            $client = new Client(
                $connection->get_api_key()
            );

            $merge_fields = $client->get_list_merge_fields(
                $connection->get_list_id()
            );

            if(!empty($saved['merge_fields'])) {
                foreach($saved['merge_fields'] as &$merge_field) {
                    list($tag, $name) = explode(':', $merge_field, 2);
                    $merge_field = $tag;
                }
            }
        } else {
            $merge_fields = [];
            foreach((array)$saved['merge_fields'] as $field) {
                list($id, $name) = explode(':', $field, 2);
                $merge_fields[] = $name;
            }
        }
        
        $form_fields = get_gravity_form_fields(
            $form_id, [
                'survey', 'html', 'page'
            ], 'not'
        );

        if($readonly) {
            $fields = $form_fields;
            $form_fields = [];
            foreach((array)$saved['form_fields'] as $field_id) {
                switch($field_id) {
                    case 'quiz_score':
                        $label =  __('Quiz Score', $textdomain);
                        break;

                    case 'quiz_result_page_url':
                        $label =  __('Quiz Result Page URL', $textdomain);
                        break;

                    default:
                        if(isset($fields[$field_id])) {
                            $label = $fields[$field_id]->label;
                        } elseif(preg_match('/^(\d+)\.\d+$/', $field_id, $matches)) {
                            $match = false;
                            list(, $parent_id) = $matches;
                            if(isset($fields[$parent_id]) && isset($fields[$parent_id]->inputs)) {
                                foreach($fields[$parent_id]->inputs as $input) {
                                    if($input['id'] == $field_id) {
                                        $label = $fields[$parent_id]->label .'('. $input['label'] .')';
                                        $match = true;
                                        break;
                                    }
                                }
                            }
        
                            if(!$match) {
                                $label = 'Field Id#'.$field_id;
                            }
                        }
                }
                
                $form_fields[] = $label;
            }
        }

        return $this->apply_field_ns(
            load_template(
                'quiz-setup-metabox-mappings' . ($readonly ? '-readonly' : ''), [
                    'merge_fields'  => $merge_fields,
                    'form_fields'   => $form_fields,
                    'saved'         => $saved
                ], [
                    'echo'          => false
                ]
            ),
            'mc4quiz'
        );
    }

    public function ajax_reset() {
        if(!empty($_POST['post_id'])) {
            $connection = new Connection($_POST['post_id']);
            $connection->reset();
            send_json(
                true, 
                load_template(
                    'connection-metabox', [
                        'apikey'    => null,
                        'list_id'   => null,
                        'list_name' => null,
                        'post_id'   => $connection->post_id
                    ], [
                        'echo' => false
                    ]
                )
            );
        } else {
            send_json(false, 'Empty post id');
        }
    }

    public function ajax_remap() {
        if(!empty($_POST['post_id'])) {
            $form_setup = new FormSetup($_POST['post_id']);
            $saved = $form_setup->meta->mc4quiz;

            send_json(
                true,
                $this->load_mappings(
                    $saved['connection_id'], $form_setup->form_id, $saved
                )
            );
        } else {
            send_json(
                false, 'Empty post id'
            );
        }
    }

    public function get_setting($name, $default = null) {
        return isset($this->settings[$name]) ? $this->settings[$name] : $default;
    }
    
    public function after_quiz_form_submission($form_setup, $entry, $form) {
        $quiz_result_page_url = $form_setup->get_quiz_result_page_url($entry['id']);
        $quiz_result = $form_setup->get_quiz_result($entry['id']);

        $mc4quiz = $form_setup->meta->mc4quiz;
        $merge_fields = [];
        
        if($mc4quiz['enable_setup'] && $mc4quiz['connection_id']) {
            foreach($mc4quiz['merge_fields'] as $i => $field) {
                list($merge_tag, $name) = explode(':', $field, 2);
                $form_field_id = $mc4quiz['form_fields'][$i];

                switch($form_field_id) {
                    case 'quiz_score':
                        $value = $quiz_result['total_score'];
                        break;

                    case 'quiz_result_page_url':
                        $value = $quiz_result_page_url;
                        break;

                    default:
                        $value = isset($entry[$form_field_id]) ? $entry[$form_field_id] : '';
                }
                
                if($merge_tag == 'email') {
                    $email = $value;
                } elseif(!empty($merge_tag) && !empty($form_field_id)) {
                    $merge_fields[$merge_tag] = $value;
                }
            }

            if(!empty($email)) {
                $subscribers[] = [
                    'email_address' => $email,
                    'status'        => 'subscribed',
                    'merge_fields'	=> $merge_fields
                ];
    
                $connection = new Connection($mc4quiz['connection_id']);
                $apikey = $connection->get_api_key();
                $list_id = $connection->get_list_id();
            
                if($apikey && $list_id) {
                    $client = new Client($apikey);
                    $client->subscribe($list_id, $subscribers);
                }
                
            }
        }
       
    }
}

function plugin() {
	return Plugin::getInstance();
}

add_action('balonka-quiz-constructed', function() {
    plugin();
});
