(function($, config) {
    var quiz = {
        saveSettings: function(data) {
            return $.ajax(config.ajax_url, {
                method: 'POST',
                dataType: 'json',
                data: {
                    action: config.save_settings,
                    data: data
                }
            }).then(function(result) {
                if(!result.success) {
                    return $.Deferred().reject();
                }
            });
        },

        connect: function(apikey) {
            return $.ajax(config.ajax_url, {
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'mc4quiz/connect',
                    /* post_id: post_id, */
                    apikey: apikey
                }
            }).then(function(response) {
                if(!response.success) {
                    return $.Deferred().reject();
                } else {
                    return $.Deferred().resolve(response.result);
                }
            });
        },

        reset: function(post_id) {
            return $.ajax(config.ajax_url, {
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'mc4quiz/reset',
                    post_id: post_id,
                }
            }).then(function(response) {
                if(!response.success) {
                    return $.Deferred().reject();
                } else {
                    return $.Deferred().resolve(response.result);
                }
            });
        },

        loadMappings: function(connection_id, form_id) {
            return $.ajax(config.ajax_url, {
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'mc4quiz/load_mappings',
                    connection_id: connection_id,
                    form_id: form_id
                }
            }).then(function(response) {
                if(!response.success) {
                    return $.Deferred().reject();
                } else {
                    return $.Deferred().resolve(response.result);
                }
            });
        },

        remap: function(post_id) {
            return $.ajax(config.ajax_url, {
                method: 'POST',
                dataType: 'json',
                data: {
                    action: 'mc4quiz/remap',
                    post_id: post_id,
                }
            }).then(function(response) {
                if(!response.success) {
                    return $.Deferred().reject();
                } else {
                    return $.Deferred().resolve(response.result);
                }
            });
        },

        event: {
            onSubmitSettings: function(e) {
                e.preventDefault();

                var $form = $(this),
                    data = $form.serialize(),
                    savingMsg = $form.data('savingMsg') || 'Saving...',
                    successMsg = $form.data('successMsg') || 'Successfully saved',
                    errorMsg = $form.data('errorMsg') || 'Error saving!',
                    $submit = $form.find('[type=submit]');

                $submit.data('text', $submit.text()).text(savingMsg).prop('disabled', true);

                donation.saveSettings(data)
                    .done(function() {
                        $submit.after('<span class="save_status success-msg" style="margin-left: 10px;color: green">' +successMsg+ '</span>');
                    })
                    .fail(function() {
                        $submit.after('<span class="save_status error_msg" style="margin-left: 10px;color: red">' +errorMsg+ '</span>');
                    })
                    .always(function() {
                        $submit.text(
                            $submit.data('text')
                        ).prop('disabled', false);

                        var t = setTimeout(function() {
                            $submit.next('span').fadeOut(400, function() {
                                $submit.next('span').remove();
                            });
                        }, 7000)
                    })
            },

            onConnect: function(e) {
                var $mc4quiz = $('.mc4quiz');
                var post_id = $mc4quiz.data('postId');
                var apikey = $(this).prev('input').val();
                var loadingMsg = $mc4quiz.data('loadingMsg') || 'Loading...';
                var errorMsg = $mc4quiz.data('errorMsg') || 'Error loading!';

                var $loading = $('<div>', {
                    'class': '--js-div loading',
                    text: loadingMsg
                });

                var $error = $('<div>', {
                    'class': '--js-div error',
                    text: errorMsg
                });

                $mc4quiz.find('.--js-div').remove();
                $mc4quiz.append($loading);

                quiz.connect(apikey)
                    .done(function(result) {
                        var $result = $('<div>', {
                            class: '--js-div --js-result-div',
                            html: result
                        });
                        
                        $mc4quiz.append($result);
                        $loading.remove();
                    })
                    .fail(function() {
                        $mc4quiz.append($error);
                        $loading.remove();
                    })
            },

            onReset: function(e) {
                var $mc4quiz = $('.mc4quiz');
                var post_id = $mc4quiz.data('postId');
                var loadingMsg = $mc4quiz.data('loadingMsg') || 'Loading...';
                var errorMsg = $mc4quiz.data('errorMsg') || 'Error loading!';

                var $loading = $('<div>', {
                    'class': '--js-div loading',
                    text: loadingMsg
                });

                var $error = $('<div>', {
                    'class': '--js-div error',
                    text: errorMsg
                });

                $mc4quiz.find('.--js-div').remove();
                $mc4quiz.append($loading);

                quiz.reset(post_id)
                    .done(function(result) {
                        var $result = $(result);
                        if($result.is('.mc4quiz')) {
                            $mc4quiz.replaceWith($result);
                        } else {
                            $mc4quiz.html($result);
                        }
                        
                        $loading.remove();
                    })
                    .fail(function() {
                        $mc4quiz.append($error);
                        $loading.remove();
                    })
            },
         
            onInputApiKey: function(e) {
                var mc4quiz = $(this).closest('.mc4quiz');
                var $btn = $(this).next();

                $btn.prop('disabled', this.value.replace(/^\s+$/, '') == '');
            },

            onSelectConnection: function(e) {
                var $select = $(this);
                var $mc4quizsetup = $select.closest('.mc4quiz-setup');
                var form_id = $mc4quizsetup.data('formId');
                var connection_id = $select.val();
                var loadingMsg = $mc4quizsetup.data('loadingMsg') || 'Loading...';
                var errorMsg = $mc4quizsetup.data('errorMsg') || 'Error loading!';

                var $loading = $('<div>', {
                    'class': '--js-div loading',
                    text: loadingMsg
                });

                var $error = $('<div>', {
                    'class': '--js-div error',
                    text: errorMsg
                });

                $mc4quizsetup.find('.--js-div').remove();
                $mc4quizsetup.append($loading);

                quiz.loadMappings(connection_id, form_id)
                    .done(function(result) {
                        $.each(result, function(sel, html) {
                            var $item = $mc4quizsetup.find(sel);
                            $item.length ? $item.replaceWith(html) : $mc4quizsetup.append(html);
                        })

                        $loading.remove();
                    })
                    .fail(function() {
                        $mc4quizsetup.append($error);
                        $loading.remove();
                    })
            },

            onRemap: function(e) {
                var $btn = $(this);
                var $mc4quizsetup = $btn.closest('.mc4quiz-setup');
                var post_id = $mc4quizsetup.data('postId');
                var loadingMsg = $mc4quizsetup.data('loadingMsg') || 'Loading...';
                var errorMsg = $mc4quizsetup.data('errorMsg') || 'Error loading!';
                var $mappingReadonlyRow = $mc4quizsetup.find('.mapping-readonly-row');

                var $loading = $('<div>', {
                    'class': '--js-div loading',
                    text: loadingMsg
                });

                var $error = $('<div>', {
                    'class': '--js-div error',
                    text: errorMsg
                });

                $mc4quizsetup.find('.--js-div').remove();
                $mc4quizsetup.append($loading);

                quiz.remap(post_id)
                    .done(function(result) {
                        $mappingReadonlyRow.replaceWith($(result));
                        $loading.remove();
                    })
                    .fail(function() {
                        $mc4quizsetup.append($error);
                        $loading.remove();
                    })
            }
        }
    };

    $(function() {
        //$('#donation-main').on('submit', '.donation-settings form', donation.event.onSubmitSettings);
        $('.mc4quiz').on('change, input, blur', 'input[name=apikey]', quiz.event.onInputApiKey);
        $('.mc4quiz').on('click', 'button[name=connect]', quiz.event.onConnect);
        $('.mc4quiz').on('click', 'button[name=reset]', quiz.event.onReset);
        $('.mc4quiz-setup').on('change', '#mc4quiz_connection_id', quiz.event.onSelectConnection);
        $('.mc4quiz-setup').on('click', 'button[name=remap]', quiz.event.onRemap);
        
        $('.mc4quiz .reveal').on('click', '.btn-reveal, .block', function(e) {
            var $reveal = $(this).closest('.reveal');
            var reveal = $reveal.data('reveal') || 'full';
            var $target = $(e.target);

            if(reveal == 'partial') {
                $reveal.attr('data-reveal', 'full');
                $reveal.data('reveal', 'full');
            } else if($target.is('.btn-reveal')) {
                $reveal.attr('data-reveal', 'partial');
                $reveal.data('reveal', 'partial');
            }
        })
    })
})(jQuery, window.mc4quiz_config || {});