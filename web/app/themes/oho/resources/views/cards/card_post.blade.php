
<a href="{!! $link !!}" class="card card-post">

	<div class="card-post__inner">

		<div class="card-post__image">

			@if ( $featured_image )
			<img src="{!! $featured_image['sizes']['large'] !!}" alt="{!! $featured_image['alt'] !!}">
			@endif

		</div>

		<div class="card-post__title">{!! $title !!}</div>

		@if ( $excerpt )
		<div class="card-post__excerpt">{!! $excerpt !!}</div>
		@endif

	</div>

</a>
