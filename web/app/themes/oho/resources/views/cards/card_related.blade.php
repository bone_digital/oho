
<a href="{!! $link !!}" class="card card-related">

	<div class="card-related__inner">

		<div class="card-related__image">

		@if ( $featured_image )
		<img src="{!! $featured_image['sizes']['medium'] !!}" alt="{!! $featured_image['alt'] !!}">
		@endif

		</div>

		<div class="card-related__date">{!! $date !!}</div>

		<div class="card-related__title">{!! $title !!}</div>

	</div>

</a>
