<div class="module module-testimonial @if(!empty($image_position)){!! $image_position !!}@endif">

	<div class="module-testimonial__inner">

		<div class="module-testimonial__image">

			<div class="module-testimonial__image-wrapper image-wrapper">

				{!! App::generateImgTag($image, 'card', 'module-testimonial__image-element') !!}

			</div>

		</div>

		<div class="module-testimonial__content-wrapper">

			<div class="module-testimonial__content content">

				{!! $quote !!}

			</div>

			@if($author_name)
			<div class="module-testimonial__name">

				{!! $author_name !!}

			</div>
			@endif

			@if($position)
			<div class="module-testimonial__position">

				{!! $position !!}

			</div>
			@endif

		</div>

	</div>

</div>
