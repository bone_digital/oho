<div class="module module-video-text {!! $video_position !!} {!! $video_width !!}">

	<div class="module-video-text__inner">

		<div class="module-video-text__image">

			<a href="{!! $video_url !!}" data-lity class="module-video-text__image-wrapper image-wrapper">

				{!! App::generateImgTag($image, 'card', 'module-video-text__image-element') !!}

			</a>

		</div>

		<div class="module-video-text__content-wrapper">

			<div class="module-video-text__content content">

				{!! $content !!}

			</div>

			@if($button)
			<a class="button button--blue" href="{!! $button['url'] !!}">{!! $button['title'] !!}</a>
			@endif

		</div>

	</div>

</div>
