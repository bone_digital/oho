<div class="module module-logos">

	<div class="module-logos__inner">

		<div class="module-logos__title">

			<h2>{!! $title !!}</h2>

			@if($description)
			<div class="module-logos__description content">

				{!! $description !!}

			</div>
			@endif

		</div>

		<div class="module-logos__logos">

			@foreach($logos as $logo)
			<a class="module-logos__logo" target="_blank" href="{!! $logo['link'] !!}">

				{!! App::generateImgTag($logo['image'], 'card', 'module-logos__image-element') !!}

			</a>
			@endforeach

		</div>

	</div>

</div>
