<div class="post-module post-module-image">

	<div class="post-module-image__inner">

		{!! App::generateImgTag($image, 'card', 'module-video-text__image-element') !!}

		@if($caption)
		<div class="post-module-image__caption">{!! $caption !!}</div>
		@endif

	</div>

</div>
