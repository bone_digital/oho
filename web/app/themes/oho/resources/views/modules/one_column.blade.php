<div class="module module-one-column">

		<div class='module-one-column__inner'>

			<div class="module-one-column__title">

				@if($title)
				<h2>{!! $title !!}</h2>
				@endif

				@if($sub_title)
				<h5>{!! $sub_title !!}</h5>
				@endif

			</div>

			<div class="module-one-column__columns">

				@if($column_1)
				<div class="module-one-column__column content">

					<div class="module-one-column__content content">

						{!! $column_1 !!}

					</div>

					@if($column_1_button)
					<a class='button {!! $button_style !!}' href="{!! $column_1_button['url'] !!}">{!! $column_1_button['title'] !!}</a>
					@endif

				</div>
				@endif

			</div>

		</div>

</div>
