<div class="module module-image-text {!! $image_position !!} {!! $image_width !!}">

	<div class="module-image-text__inner">

		<div class="module-image-text__image">

			<div class="module-image-text__image-wrapper image-wrapper">

				{!! App::generateImgTag($image, 'card', 'module-image-text__image-element') !!}

			</div>

		</div>

		<div class="module-image-text__content-wrapper">

			<div class="module-image-text__content content">

				{!! $content !!}

			</div>

			@if($button)
			<a class="button {!! $button_style !!}" href="{!! $button['url'] !!}">{!! $button['title'] !!}</a>
			@endif

		</div>

	</div>

</div>
