<div class="module module-accordion">

	<div class="module-accordion__inner">

		<div class="module-accordion__list">

			@foreach($items as $item)

			<div class="module-accordion__item">

				<a href="#" data-no-swup class="module-accordion__trigger"><h2>{!! $item['title'] !!}</h2>@include('svgs.accordion')</a>

				<div class="module-accordion__content-wrapper">

					<div class="module-accordion__content content">

						{!! $item['content'] !!}

					</div>

					@if($item['button'])
					<a class="button {!! $item['button_style'] !!}" href="{!! $item['button']['url'] !!}">{!!  $item['button']['title'] !!}</a>
					@endif

				</div>

			</div>
			@endforeach

		</div>

	</div>

</div>
