<div class="post-module post-module-quote">

	<div class="post-module-quote__inner">

		<div class="post-module-quote__quote">{!! $quote !!}</div>

		@if($attribution)
		<div class="post-module-quote__attribution">{!! $attribution !!}</div>
		@endif

	</div>

</div>
