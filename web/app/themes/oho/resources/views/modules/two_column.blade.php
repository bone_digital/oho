<div class="module module-two-column">

		<div class='module-two-column__inner'>

			<div class="module-two-column__title">

				@if($title)
				<h2>{!! $title !!}</h2>
				@endif

				@if($sub_title)
				<h5>{!! $sub_title !!}</h5>
				@endif

			</div>

			<div class="module-two-column__columns">

				@if($column_1)
				<div class="module-two-column__column content">

					<div class="module-two-column__content content">

						{!! $column_1 !!}

					</div>

					@if($column_1_button)
					<a class='button {!! $button_1_style !!}' href="{!! $column_1_button['url'] !!}">{!! $column_1_button['title'] !!}</a>
					@endif

				</div>
				@endif

				@if($column_2)
				<div class="module-two-column__column content">

					<div class="module-two-column__content content">

						{!! $column_2 !!}

					</div>

					@if($column_2_button)
					<a class='button {!! $button_2_style !!}' href="{!! $column_2_button['url'] !!}">{!! $column_2_button['title'] !!}</a>
					@endif

				</div>
				@endif

			</div>

		</div>
</div>
