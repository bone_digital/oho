<div class="module module-three-column">

		<div class='module-three-column__inner'>

			<div class="module-three-column__title">

				@if($title)
				<h2>{!! $title !!}</h2>
				@endif

				@if($sub_title)
				<h5>{!! $sub_title !!}</h5>
				@endif

			</div>

			<div class="module-three-column__columns">

				@if($column_1)
				<div class="module-three-column__column content">

					<div class="module-three-column__content content">

						{!! $column_1 !!}

					</div>

					@if($column_1_button)
					<a class='button {!! $button_1_style !!}' href="{!! $column_1_button['url'] !!}">{!! $column_1_button['title'] !!}</a>
					@endif

				</div>
				@endif

				@if($column_2)
				<div class="module-three-column__column content">

					<div class="module-three-column__content content">

						{!! $column_2 !!}

					</div>

					@if($column_2_button)
					<a class='button {!! $button_2_style !!}' href="{!! $column_2_button['url'] !!}">{!! $column_2_button['title'] !!}</a>
					@endif

				</div>
				@endif

				@if($column_3)
				<div class="module-three-column__column content">

					<div class="module-three-column__content content">

						{!! $column_3 !!}

					</div>

					@if($column_3_button)
					<a class='button {!! $button_3_style !!}' href="{!! $column_3_button['url'] !!}">{!! $column_3_button['title'] !!}</a>
					@endif

				</div>
				@endif

			</div>

		</div>

</div>
