<div class="post-module post-module-slider">

	<div class="post-module-slider__inner">

		<div class="post-module-slider__slider owl-carousel">
			@foreach($images as $image)

			<div class="post-module-slider__slide">

				<div class="post-module-slider__image-wrapper">

					{!! App::generateImgTag($image['image'], 'card', 'module-video-text__image-element') !!}

				</div>

				@if($image['caption'])
				<div class="post-module-slider__caption">{!! $image['caption'] !!}</div>
				@endif

			</div>
			@endforeach

		</div>

	</div>

</div>
