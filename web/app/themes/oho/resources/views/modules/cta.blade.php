<div class="module module-image-text @if ($image){!! $image_position !!} {!! $image_width !!}@endif">

	<div class="module-image-text__inner">
		
		@if ($image)
		<div class="module-image-text__image">

			<div class="module-image-text__image-wrapper image-wrapper">
				{!!
					App::generateImgTag(
						$image,
						'card',
						'module-image-text__image-element'
					)
				!!}
			</div>

		</div>
		@endif

		<div class="module-image-text__content-wrapper">

			<div class="module-image-text__content content">
				{!! $content !!}
			</div>

			<button class="popup-{!! $button_popup_trigger !!}-trigger button {!! $button_style !!}"
				data-no-swup="true"
				type="button">
				{!! $button !!}
			</button>

		</div>

	</div>

</div>
