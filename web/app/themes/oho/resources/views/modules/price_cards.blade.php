<div class="module module-cards">

	<div class="module-cards__inner">

		<div class="module-cards__title">

			<h2>{!! $title !!}</h2>

			<div class="module-cards__description content">

				{!! $description !!}

			</div>

			<div class="module-cards__slider-wrapper">

				<div class="module-cards__slider owl-carousel">

					@foreach($cards as $card)
					<div class="module-cards__card">

						<div class="module-cards__card-inner">

							<div class="module-cards__card-title">{!! $card['card_title'] !!}</div>

							<div class="module-cards__card-price">{!! $card['card_price'] !!}</div>

							<div class="module-cards__card-description">{!! $card['card_description'] !!}</div>

							<a href="#" data-no-swup class="popup-trigger button button--orange">Try it for free</a>

							<div class="module-cards__card-footer">{!! $card['card_footer'] !!}</div>

						</div>

					</div>
					@endforeach

				</div>

			</div>

			<div class="module-cards__cards cards-{{ count($cards) }}">

				@foreach($cards as $card)
				<div class="module-cards__card">

					<div class="module-cards__card-inner">

						<div class="module-cards__card-title">{!! $card['card_title'] !!}</div>

						<div class="module-cards__card-price">{!! $card['card_price'] !!}</div>

						<div class="module-cards__card-description">{!! $card['card_description'] !!}</div>

						<a href="#" data-no-swup class="popup-trigger button button--orange">Try it for free</a>

						<div class="module-cards__card-footer">{!! $card['card_footer'] !!}</div>

					</div>

				</div>
				@endforeach

			</div>

			<div class="module-cards__footer content">

				{!! $footer_text !!}

			</div>

		</div>

	</div>

</div>
