<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://use.typekit.net/sdl5pjj.css">
	{!! App::googleAnalytics() !!}

	@if(env('WP_ENV') == 'production')
		<!-- Google Tag Manager, remove on dev/staging -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-MFMC54K');</script>
		<!-- End Google Tag Manager -->

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '145842390848407');
		fbq('track', 'PageView');

		window.addEventListener('DOMContentLoaded', function(){
			var body = document.body;
			if ( ((body.classList.contains('page-id-1477')) || (body.classList.contains('page-id-1386'))) && (!body.classList.contains('logged-in')) ) {
				fbq('track', 'Lead');
			}
		});
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=145842390848407&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->

		<script type="text/javascript">
			_linkedin_partner_id = "3730652";
			window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
			window._linkedin_data_partner_ids.push(_linkedin_partner_id);
		</script><script type="text/javascript">
			(function(l) {
				if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])};
					window.lintrk.q=[]}
				var s = document.getElementsByTagName("script")[0];
				var b = document.createElement("script");
				b.type = "text/javascript";b.async = true;
				b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";;
				s.parentNode.insertBefore(b, s);})(window.lintrk);
		</script>
	@endif

	@php wp_head() @endphp

	<script>
		jQuery(document).on('gform_confirmation_loaded', function(event, formId) {
			if( formId != 3 )
			{
				return;
			}
			window.lintrk('track', { conversion_id: 6593516 });
		});
	</script>


<meta name="facebook-domain-verification" content="b8qlms102pbnds1h36gtch8vuu8xrh" />
</head>
