@if (!is_page_template('views/template-blank.blade.php'))

@include('popups.quiz_popup')
@include('popups.newsletter_popup')
@include('popups.cta_popup')

<div class="footer-cta">

	<div class="inner-wrapper">

		<div class="footer-cta__inner">
				
				@if ($show_default_footercta)

				<h3>{!! $footer_cta !!}</h3>

				<div class="footer-cta__right">

					<a href="#" data-no-swup class="popup-trigger footer-cta__button button button--orange">Try it for free</a>
					<div class="footer-cta__login">Already have an account? <a target="_blank" href="{!! $login_link !!}">Log in here</a></div>

				</div>

				@elseif (!$show_default_footercta && $footer_cta_button)

				<h3>{!! $footer_cta !!}</h3>

				<div class="footer-cta__right">
					
					<a 
						href="{!! $footer_cta_button['url'] !!}" 
						target="{!! $footer_cta_button['target'] !!}" 
						class="footer-cta__button button button--orange">
						{!! $footer_cta_button['title'] !!}
					</a>

				</div>

				@else

				<h3 class="full-width">{!! $footer_cta !!}</h3>

				@endif

		</div>

		@if (!empty($footer_images))
		<div class="footer-awards">
			
			@if ($footer_awards_heading)
			<h2>{!! $footer_awards_heading !!}</h2>
			@endif
			
			<div class="module-logos__logos">

			@foreach($footer_images as $footer_image)

				@if (!empty($footer_image['footer_image']))
				
					@if (!empty($footer_image['footer_image_link']))
						<a class="module-logos__logo" target="{!! $footer_image['footer_image_link']['target'] !!}" href="{!! $footer_image['footer_image_link']['url'] !!}">
							{!! App::generateImgTag($footer_image['footer_image'], 'card', 'module-logos__image-element') !!}
						</a>
					@else
						<div class="module-logos__logo">
							{!! App::generateImgTag($footer_image['footer_image'], 'card', 'module-logos__image-element') !!}
						</div>
					@endif

				@endif

			@endforeach

			</div>

		</div>
		@endif

	</div>

</div>


<footer class="footer">

	<div class="inner-wrapper">

		<div class="footer__inner">

			<a class="footer__logo" href="{!! home_url() !!}"></a>

				<div class="footer__nav">

					<div class="footer__column">

						@if (has_nav_menu('footer_one_navigation'))
						{!! wp_nav_menu(['theme_location' => 'footer_one_navigation', 'menu_class' => 'nav']) !!}
						@endif

					</div>

					<div class="footer__column">

						@if (has_nav_menu('footer_two_navigation'))
						{!! wp_nav_menu(['theme_location' => 'footer_two_navigation', 'menu_class' => 'nav']) !!}
						@endif

					</div>

					<div class="footer__column">

						@if (has_nav_menu('footer_three_navigation'))
						{!! wp_nav_menu(['theme_location' => 'footer_three_navigation', 'menu_class' => 'nav']) !!}
						@endif

					</div>

					<div class="footer__column">

						@if (has_nav_menu('footer_four_navigation'))
						{!! wp_nav_menu(['theme_location' => 'footer_four_navigation', 'menu_class' => 'nav']) !!}
						@endif

					</div>

					<div class="footer__socials">

						<div class="menu-header">Connect</div>

						@if($facebook_link)
						<a target="_blank" class="footer__facebook" href="{!! $facebook_link !!}"><span></span>Facebook</a>
						@endif

						@if($twitter_link)
						<a target="_blank" class="footer__twitter" href="{!! $twitter_link !!}"><span></span>Twitter</a>
						@endif

						@if($linkedin_link)
						<a target="_blank" class="footer__linkedin" href="{!! $linkedin_link !!}"><span></span>LinkedIn</a>
						@endif

						@if($youtube_link)
						<a target="_blank" class="footer__youtube" href="{!! $youtube_link !!}"><span></span>Youtube</a>
						@endif

					</div>

				</div>

		</div>

		<div class="footer__copyright">

			© Copyright <?php echo date("Y"); ?> dutyof.care Solutions Pty.

		</div>

	</div>
	
	<script type="text/javascript"> _linkedin_partner_id = "3160348"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=3160348&fmt=gif" /> </noscript>

</footer>
@endif