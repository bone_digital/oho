@if (!is_page_template('views/template-blank.blade.php'))

<header class="header">

	<div class="inner-wrapper">

		<div class="header__inner">

			<div class="header__left">

				<a class="header__logo" href="{!! home_url() !!}"></a>

				<nav class="header__nav">

					@if (has_nav_menu('primary_navigation'))
					{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
					@endif

				</nav>

			</div>

			<div class="header__actions">

				<a target="_blank" class="button button--inverse" href="{!! $login_link !!}">Log in</a>

				<a class="popup-trigger-header button button--orange" href="#" data-no-swup>Try it for free</a>

				<a class="header__menu-trigger menu-trigger" href="#" data-no-swup><span class="bar bar--1"></span><span class="bar bar--2"></span><span class="bar bar--3"></span></a>

			</div>

		</div>

	</div>

</header>

<div class="mobile-menu">

	<div class="inner-wrapper">

		<div class="mobile-menu__inner">

			<div class="mobile-menu__actions">

				<a class="button button--inverse" href="#">Log in</a>

				<a class="popup-trigger-header button button--orange" href="#">Try it for free</a>

			</div>

			<div class="mobile-menu__nav">

				@if (has_nav_menu('primary_navigation'))
				{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
				@endif

			</div>

		</div>

	</div>

</div>
@endif