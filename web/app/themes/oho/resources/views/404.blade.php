@extends('layouts.app')

@section('content')
<div class="error-wrapper">

	<div class="inner-wrapper">

		<div class="error-wrapper__inner">

			<h1>Oho no!</h1>
			<h3>Page Not Found</h3>
			<a class="button button--orange" href="{!! home_url() !!}">Back to Home</a>

		</div>

	</div>

</div>

@endsection
