@if(!empty($cta_popup) && is_array($cta_popup))
	<div id="cta-popup" class="cta-popup">

		<div class="newsletter-popup__background"></div>

		<div class="inner-wrapper newsletter-popup__inner-wrapper">

			<div class="newsletter-popup__inner">

				<button class="popup-cta-trigger"></button>

				<div class="newsletter-popup__title">

					@if (!empty($cta_popup['heading']))
						<h3>{!! $cta_popup['heading'] !!}</h3>
					@endif
					@if( !empty($cta_popup['subheading']) )
						<p>
							{!! $cta_popup['subheading'] !!}
						</p>
					@endif

				</div>

					<div class="content">

						@if(!empty($cta_popup['form_id']))
							@php(gravity_form(
								$cta_popup['form_id'],
								false,
								false,
								false,
								null,
								true
							))
						@endif

						@if( !empty($cta_popup['content']) )
							{!! $cta_popup['content'] !!}
						@endif
					</div>

			</div>

		</div>

	</div>
@endif
