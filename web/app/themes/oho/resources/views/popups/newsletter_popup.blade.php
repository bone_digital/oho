<div id="newsletter-popup" class="newsletter-popup">

	<div class="newsletter-popup__background"></div>

	<div class="inner-wrapper newsletter-popup__inner-wrapper">

		<div class="newsletter-popup__inner">

			<a data-no-swup class="popup-trigger" href="#"></a>

			<div class="newsletter-popup__title">

				<h3>Try now for free</h3>

				<p>Give Oho a try for 7 days.<br>
					No credit card required.</p>

				<div class="newsletter-popup__errors"></div>

			</div>

			<form action="/" class="newsletter-popup__form">

				<div class="input-wrapper"><input type="text" name="org-name" placeholder="Enter your organisation name" required></div>
				<div class="input-wrapper"><input type="text" name="full-name" placeholder="Enter your full name" required></div>
				<div class="input-wrapper"><input type="email" name="email" placeholder="Enter your business email address" required></div>
				<div class="input-wrapper"><input type="password" name="pwd" placeholder="Create a password (min 8 characters)" required></div>
				<span>
					<button class="button button--orange" type="submit">Try it for free</button>
					<p>By signing up, you are agreeing to our Terms and Privacy Policy.</p>
				</span>

			</form>

			<div class="newsletter-popup__login">Already have an account? <a target="_blank" href="{!! $login_link !!}">Log in here</a></div>

		</div>

	</div>

</div>
