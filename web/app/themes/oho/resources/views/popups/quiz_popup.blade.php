
<div id="popup-form-wrapper">
	<div id="quiz-popup" class="quiz-popup cta-popup @if ($show_quiz_popup) is-enabled @endif" data-show-popup="sitewide--{!! $popup_entire_site !!}">
		<div class="newsletter-popup__background"></div>

		<div class="inner-wrapper newsletter-popup__inner-wrapper">
			<div class="newsletter-popup__inner">

				<button class="popup-quiz-trigger"></button>

				<div class="newsletter-popup__title">
					@if (!empty($popup_heading))
					<h3>{!! $popup_heading !!}</h3>
					@endif
					@if( !empty($popup_subheading) )
					<h4>{!! $popup_subheading !!}</h4>
					@endif
					@if( !empty($popup_copy) )
					<p>{!! $popup_copy !!}</p>
					@endif
				</div>

				<div class="content">
					@if(!empty($quiz_selection))
					@php(gravity_form(
					$quiz_selection,
					false,
					false,
					false,
					null,
					true
					))
					@endif

					@if( !empty($quiz_disclaimer) )
					<p class="disclaimer">{!! $quiz_disclaimer !!} <a href="{!! $quiz_disclaimer_link['url'] !!}">{!! $quiz_disclaimer_link['title'] !!}</a>.</p>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
