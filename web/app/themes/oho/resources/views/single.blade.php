@extends('layouts.app')

@section('content')

	<div class="blog-single-wrapper">

		<div class="inner-wrapper">

			<div class="blog-single-wrapper__inner">

				<h1 class="page-title">{!! $title !!}</h1>

				<div class="blog-single-wrapper__details">

					<div class="blog-single-wrapper__date">{!! $date !!}</div>
					@if ($read_time)
					<span></span>
					@endif
					<div class="blog-single-wrapper__read-time">{!! $read_time !!}</div>

				</div>

				<div class="blog-single-wrapper__featured-image">

					{!! App::generateImgTag($featured_image, 'card', 'blog-single-wrapper__image-element') !!}

				</div>

				<div class="blog-single-wrapper__post-builder">

					@include('partials.page-builder')

				</div>

			</div>

		</div>

	</div>

	<div data-wp class="section section-posts">

		<div class="inner-wrapper">

			<div class="section-posts__inner">

				<div class="section-posts__title">

					<h2>Keep reading</h2>

				</div>

				<div class="section-posts__posts">

					@foreach ($latest_posts as $latest_post)
						@include('cards.card_related', $latest_post)
					@endforeach

				</div>

			</div>

		</div>

	</div>

@include('sections.newsletter_signup')

@endsection
