<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
	  <!-- Google Tag Manager (noscript) -->
	  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MFMC54K"
						height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	  <!-- End Google Tag Manager (noscript) -->
    @php do_action('get_header') @endphp

	  @if(env('WP_ENV') == 'production')
		  <noscript>
			  <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=3730652&fmt=gif"; />
		  </noscript>
	  	@endif

    @include('partials.header')

    <div id="body-wrapper" class="body-wrapper">

		@yield('content')
    </div>

		@if ($show_quiz_sidebar)
		@if ( $sidebar_entire_site )
		<div class="sidebar-quiz-cta">
			<a id="quiz-cta-button" class="button" href="{!! $sidebar_button['url'] !!}" data-no-swup>{!! $sidebar_button['title'] !!}</a>
		</div>
		@elseif ( (!$sidebar_entire_site) && (is_front_page()) )
		<div class="sidebar-quiz-cta">
			<a id="quiz-cta-button" class="button" href="{!! $sidebar_button['url'] !!}" data-no-swup>{!! $sidebar_button['title'] !!}</a>
		</div>
		@endif
		@endif

    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
