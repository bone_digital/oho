@extends('layouts.app')

@section('content')

<div class="blog-archive-wrapper">

	<div class="inner-wrapper">

		<h1 class="page-title">The latest from Oho</h1>

		<div class="blog-archive-wrapper__inner">

			<div class="blog-archive-wrapper__posts">

				@foreach ($posts as $post)
					@include('cards.card_post', $post)
				@endforeach

				<div class="card-post empty-div"></div>

			</div>

			<div class="blog-archive-wrapper__paging">{!! $paging !!}</div>

		</div>

	</div>

</div>

@include('sections.newsletter_signup')

@endsection
