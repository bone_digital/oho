{{--
  Template Name: Blank
--}}

@extends('layouts.app')

@section('content')

<div class="blank-page-template" data-wp>

@php
if ( have_posts() ) :
    while ( have_posts() ) : the_post();
        the_content();
    endwhile;
endif;
@endphp
</div>

@endsection
