<div data-wp class="section section-posts {!! $background_colour !!}">

	<div class="inner-wrapper">

		<div class="section-posts__inner">

			<div class="section-posts__title">

				<h2>The latest from Oho</h2>

			</div>

			<div class="section-posts__posts">

				@foreach ($latest_posts as $latest_post)
					@include('cards.card_related', $latest_post)
				@endforeach

			</div>

			<a href="{!! home_url() !!}/blog/" class="button button--inverse">Visit the blog</a>

		</div>

	</div>

</div>
