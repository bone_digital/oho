<div data-wp class="section section-{!! $name !!} {!! $background_colour !!}">
	<div class="inner-wrapper">

		@if($modules)
			@foreach($modules as $module)
				@include('modules.' . $module['name'], $module['data'])
			@endforeach
		@endif

	</div>
</div>
