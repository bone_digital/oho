<div class="section section-image-slider">

	<div class='section-image-slider__inner'>

		<div class="section-image-slider__slider owl-carousel">

		@foreach($images as $image)
			{!! App::generateImgTag($image, 'full') !!}
		@endforeach

		</div>

	</div>

</div>
