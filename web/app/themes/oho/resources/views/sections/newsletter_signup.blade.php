<div class='section section-newsletter'>

	{!! App::generateImgTag($newsletter_background_image, 'full', 'section-newsletter__image-element') !!}

	<div class="inner-wrapper">

		<div class="section-newsletter__inner">

			<div class="section-newsletter__content content">

				<h2>Get the latest from Oho sent straight to your inbox.</h2>

				<?php gravity_form(1, false, false, false, null, true); ?>

			</div>

		</div>

	</div>

</div>
