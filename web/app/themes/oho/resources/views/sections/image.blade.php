<div class="section section-{!! $name !!}">

		<div class='section-image__inner'>

			@if($image)
				{!! App::generateImgTag($image, 'full') !!}
			@endif

		</div>

</div>
