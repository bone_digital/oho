@extends('layouts.app')

@section('content')

<div data-wp class="page-header {!! $page_header_colour !!}" style="background-image: url({!! $page_header_image !!}); background-size: cover; background-position: center; background-repeat: no-repeat;">

	<div class="inner-wrapper">

		<div class="page-header__inner">

			<h1>{!! $title !!}</h1>

		</div>

	</div>

</div>

<div class="page-builder-wrapper">

	@include('partials.page-builder')

</div>

@endsection
