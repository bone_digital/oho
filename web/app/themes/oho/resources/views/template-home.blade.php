{{--
  Template Name: Home
--}}

@extends('layouts.app')

@section('content')

<div class="home-header" data-wp>

	<div class="inner-wrapper">

		{!! App::generateImgTag($home_page_desktop_image, 'full', 'home-header__desktop-image-element') !!}

		{!! App::generateImgTag($home_page_mobile_image, 'full', 'home-header__mobile-image-element') !!}

		<div class="home-header__inner">

			<div class="home-header__content">

				{!! $home_page_content !!}

			</div>

			@if ($show_default_cta)
				<div class="home-header__actions">

					<a href="#" class="popup-trigger home-header__button button button--orange">Try it for free</a>
					<div class="home-header__login">Already have an account? <a target="_blank" href="{!! $login_link !!}">Log in here</a></div>

				</div>
			@else
				@if ($home_page_button)
					<div class="home-header__actions">
						<a 
							href="{!! $home_page_button['url'] !!}" 
							target="{!! $home_page_button['target'] !!}" 
							class="home-header__button button button--orange">
							{!! $home_page_button['title'] !!}
						</a>
					</div>
				@endif
			@endif

		</div>

	</div>

</div>

<div class="page-builder-wrapper">


	@include('partials.page-builder')


</div>

@endsection
