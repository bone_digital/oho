import $ from 'jquery';
import Cookies from 'js-cookie';

export function setup_cookie_notice_popup()
{
	let popup = $('.cookie-notice');
	if( popup.length <= 0 )
	{
		//Cookie popup doesn't exist
		return;
	}

	$('button#button--accept-cookies').click(function(e){
		e.preventDefault();
		Cookies.set('cookie-notice', 'set');
		$('.cookie-notice').fadeOut('fast');
	});
}
