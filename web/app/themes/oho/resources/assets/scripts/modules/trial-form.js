export function trial_form_integration()
{

    var trial_form = document.querySelector('.newsletter-popup__form');

    trial_form.addEventListener('submit', function(e){
        e.preventDefault();

        let body = $('body');
        if( body.hasClass('loading') )
		{
			//Don't allow two submits
			return;
		}
        body.addClass('loading');

        let formData = new FormData(trial_form);

		let org_name = formData.get('org-name');
		let full_name = formData.get('full-name');
		let email = formData.get('email');
		let pwd = formData.get('pwd');

        const data = {
			data: {
				organization: { name: org_name },
				user: {
					constituent: {
						full_name: full_name,
						email: email,
					},
					password: pwd,
				},
				meta: { plan_size: 0 },
			},
			action: 'submit_trial_data',
		};

        $.post(Ajax.url, data, function(res) {
			let resDetails = JSON.parse(res);
			if( resDetails.success == true )
			{
				$('body').append('<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=3160348&conversionId=4295892&fmt=gif" />');
				window.location.href = resDetails.url;
			}
			else
			{
				body.removeClass('loading');
				$('.newsletter-popup__errors').html(resDetails.errors);
                $('.newsletter-popup__errors').slideDown();
			}
		});

    });

 }
