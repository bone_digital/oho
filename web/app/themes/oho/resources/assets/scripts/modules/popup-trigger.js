import Cookies from 'js-cookie';

export function popup_trigger()
{
	let target = $('a[href="#popup-trigger"]');
	if( target.length > 0)
	{
		target.each(function(){
			$(this).addClass('popup-trigger');
			$(this).attr('data-no-swup', 'data-no-swup');
		});
	}

	var pop_up_triggers = $('.popup-trigger');
	var pop_up = document.querySelector('#newsletter-popup');

	if (pop_up_triggers.length && pop_up) {

		var popup_active = false;

		var pop_up_inner = pop_up.querySelector('.newsletter-popup__inner');

		pop_up_triggers.each(function(){
			$(this).on('click', function(e){
				e.preventDefault();
				if (popup_active) {
					pop_up.classList.remove('active');
				} else {
					pop_up.classList.add('active');
					$('body').append('<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=3160348&conversionId=4295884&fmt=gif" />');
				}
				popup_active = !popup_active;
			});
		});

		pop_up.addEventListener('click', function(e){
			pop_up.classList.remove('active');
		});

		pop_up_inner.addEventListener('click', function(e){
			e.stopPropagation();
		});

	}
}
export function popup_trigger_header()
{
	var pop_up_triggers = $('.popup-trigger-header');
	var pop_up = document.querySelector('#newsletter-popup');

	if (pop_up_triggers.length && pop_up) {

		var popup_active = false;

		pop_up_triggers.each(function(){
			$(this).on('click', function(e){
				e.preventDefault();

				if (popup_active) {
					pop_up.classList.remove('active');
				} else {
					pop_up.classList.add('active');
					$('body').append('<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=3160348&conversionId=4295884&fmt=gif" />');
				}
				popup_active = !popup_active;
			});
		});
	}
}
export function cta_trigger() {
	var cta_popup_trigger = $('.popup-cta-trigger');
	var cta_popup = $('#cta-popup');

	if (cta_popup.length) {
		cta_popup_trigger.on('click', function() {
			if (cta_popup.hasClass('active')) {
				cta_popup.removeClass('active')
			} else {
				cta_popup.addClass('active')
			}
		});
	}
}

export function quiz_popup_trigger() {
	var pop_up_trigger = $('.popup-quiz-trigger');
	var pop_up = $('#quiz-popup');

	if (pop_up.length) {
		pop_up_trigger.off('click'); //prevent duplicate click listeners
		pop_up_trigger.on('click', () => {
			if (pop_up.hasClass('active')) {
				pop_up.removeClass('active');
				Cookies.set('quiz-popup', 'set', { expires: 7 });
			} else {
				pop_up.addClass('active')
			}
		});
	}

	var body = $('body');
	var quiz_cookie_exists = Cookies.get('quiz-popup');

	if ('sitewide--false' == pop_up.attr('data-show-popup') && body.hasClass('home') && null == quiz_cookie_exists ) {
		if (pop_up.hasClass('is-enabled')) {
			setTimeout(function() {
				pop_up.addClass('active');
			}, 1000);
		}
	}

	//remove from page with same form
	if ( body.hasClass('page-id-1361') ) {
		pop_up.remove();
	}

	if ('sitewide--true' == pop_up.attr('data-show-popup') && null == quiz_cookie_exists) {
		if ( body.hasClass('page-id-1477')
		|| body.hasClass('page-id-1386')
		|| body.hasClass('page-id-1373')
		|| body.hasClass('page-id-1361') ) {
			// nothing
		}
		else {
			if (pop_up.hasClass('is-enabled')) {
				setTimeout(function() {
					pop_up.addClass('active');
				}, 1000);
			}
		}
	}
}
