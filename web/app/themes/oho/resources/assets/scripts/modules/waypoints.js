import waypoint from 'waypoints/lib/jquery.waypoints.min';
import inview from 'waypoints/lib/shortcuts/inview.min';

export function active_section()
{
	let waypoints = $('[data-wp]').waypoint({
		handler: function (direction) {
			let _this = $(this)[0]['element'];
			let active_section = $(_this);
			active_section.addClass('active');
		},
		offset: '80%',
	});
}
