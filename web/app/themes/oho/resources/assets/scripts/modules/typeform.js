import $ from 'jquery';
import Cookies from 'js-cookie';

export function typeform_cookie()
{

	$('body').on('click', '.typeform-close', function () {
		Cookies.set('tf-form', 'seen', { expires: 30 });
	})
}