import 'owl.carousel';

export function sliders()
{
	let sliders = $('.post-module-slider__slider');

	sliders.each(function()
	{
		//Change the options here - you can use data-* tags to specify different values for different sliders
		let options = {
			items: 1,
			nav: true,
			smartSpeed: 1000,
			loop: true,
			autoHeight: true,
			margin: 30,
		};

		$(this).owlCarousel(options);
	});

	sliders = $('.module-cards__slider');

	sliders.each(function()
	{
		//Change the options here - you can use data-* tags to specify different values for different sliders
		let options = {
			items: 1,
			nav: false,
			loop: true,
			margin: 20,
			stagePadding: 20,
		};

		$(this).owlCarousel(options);
	});

	sliders = $('.section-image-slider__slider');

	sliders.each(function()
	{
		//Change the options here - you can use data-* tags to specify different values for different sliders
		let options = {
			items: 1,
			nav: false,
			loop: true,
			margin: 0,
			dots: true,
			autoplay: true,
			autoplayTimeout: 3000,
			smartSpeed: 1000,
		};

		$(this).owlCarousel(options);
	});
}
