import { update_menu } from '../modules/menu';
import { sliders } from '../modules/sliders';
import { active_section } from '../modules/waypoints';
import { popup_trigger, popup_trigger_header, quiz_popup_trigger, cta_trigger } from '../modules/popup-trigger';
import { typeform_cookie } from '../modules/typeform'

export default {
	init() {
		// JavaScript to be fired on all pages
		update_menu();
		setup_scrolling();
		setup_back_to_top();
		sliders();
		active_section();
		accordion_items();
		popup_trigger();
		cta_trigger();
		typeform_cookie();
		quiz_popup_trigger();
		popup_trigger_header();
	},
	finalize() {
		// JavaScript to be fired on all pages, after page specific JS is fired
	},
};

/**
 * Sets up the scrolled class
 */
function setup_scrolling()
{
	$(window).on('scroll', function() {
		//Add the 'scrolled' class
		let scroll = $(this).scrollTop();
		let body = $('body');
		if(scroll > 0)
		{
			body.addClass('scrolled');
		}
		else
		{
			body.removeClass('scrolled');
		}
	});
}

/**
 * Sets up a scroll to top on any back to top classes
 */
function setup_back_to_top()
{
	$('.back-to-top').click(function (e){
		e.preventDefault();
		$('html,body').animate({
			scrollTop: 0,
		}, 'slow');
	});
}

/**
 * Accordian functionality
 */
function accordion_items()
{

	var accordion_items = document.querySelectorAll('.module-accordion__item');

	if (accordion_items.length) {

		accordion_items.forEach(function(accordion_item){

			var accordion_item_content = accordion_item.querySelector('.module-accordion__content-wrapper');

			accordion_item.addEventListener('click', function(e){
				e.preventDefault();
				accordion_item.classList.toggle('active');
				$(accordion_item_content).slideToggle();
			})
		});

	}
}
