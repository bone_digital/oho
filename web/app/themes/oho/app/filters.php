<?php

namespace App;

add_filter('wp_footer', function() {
	if(is_page('quiz-result'))
	{
		if( env('WP_ENV') == 'production' ) :
		?>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				window.lintrk('track', { conversion_id: 6593508 });
			});
		</script>
		<?php
		endif;
	}
});

//add_filter( 'gform_confirmation_loaded_3', function($confirmation, $form, $entry, $ajax) {
//	if ( ! is_string( $confirmation ) ) {
//		return $confirmation;
//	}
//
//	$code = "alert(78);";
//
//	$confirmation .= \GFCommon::get_inline_script_tag($code);
//
//	return $confirmation;
//}, 10, 4 );

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Add animation class for swupjs */
	$classes[] = 'transition-fade';

	$header_colour = get_field('website_header_colour');
	if( $header_colour )
	{
		$classes[] = $header_colour;
	}

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

function form_submit_button( $button, $form )
{
	$dom = new \DOMDocument();
	$dom->loadHTML('<?xml encoding="utf-8" ?>' . $button);
	$input = $dom->getElementsByTagName('input')->item(0);
	$label = $input->getAttribute('value');

	return "<button class='button gform_button' id='gform_submit_button_{$form['id']}'><span>{$label}</span></button>";
}
add_filter( 'gform_submit_button', 'App\form_submit_button', 10, 2 );

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);
