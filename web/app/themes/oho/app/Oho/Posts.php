<?php
namespace App\Oho;

use App;

class Posts
{
	
	/**
	* Gets the details for the front end card from the post ID
	*
	* @param $post_id
	* @return array
	*/
	public static function GetCardDetails( $post_id )
	{
		$card = [];
		
		$title = get_the_title( $post_id );
		$link = get_the_permalink( $post_id );
		$excerpt = get_field( 'excerpt', $post_id );
		$featured_image = get_field( 'featured_image', $post_id );
		$date = get_the_date('j M');
		
		$card = [
			'title'	=> $title,
			'link'	=> $link,
			'excerpt' => $excerpt,
			'featured_image' => $featured_image,
			'date' => $date
		];
		
		return $card;
	}
	
	public static function GetPosts()
	{
		
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args = [
			'post_type'	=> 'post',
			'post_status' => 'publish',
			'orderby' => 'date',
			'order' => 'desc',
			'paged' => $paged
		];
		$query = new \WP_Query($args);
		$items = false;
		if( $query->have_posts() )
		{
			$items = [];
			while($query->have_posts())
			{
				$query->the_post();
				$items[] = self::GetCardDetails(get_the_ID());
			}
		}
		return $items;
	}
	
	public static function GetLatestPosts( $post_id )
	{
		
		$args = [
			'post_type'	=> 'post',
			'post_status' => 'publish',
			'orderby' => 'date',
			'order' => 'desc',
			'posts_per_page' => 4,
			'post__not_in' => array($post_id)
		];
		$query = new \WP_Query($args);
		$items = false;
		if( $query->have_posts() )
		{
			$items = [];
			while($query->have_posts())
			{
				$query->the_post();
				$items[] = self::GetCardDetails(get_the_ID());
			}
		}
		return $items;
	}
}