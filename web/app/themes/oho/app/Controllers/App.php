<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
	/**
	 * Returns the image title tag from an ID or image field in ACF
	 *
	 * @param $image
	 * @return mixed|string
	 */
	public static function getImageTitle($image)
	{
		$title = '';
		if( null == $image )
		{
			//Empty
			return '';
		}

		if( is_array($image) )
		{
			if( array_key_exists('title', $image) )
			{
				$title = $image['title'];
			}
		}
		else if( is_integer($image) )
		{
			$title = get_the_title($image);
		}

		return $title;
	}

	public function FacebookLink()
	{
		return get_field('facebook_link', 'options');
	}

	public function TwitterLink()
	{
		return get_field('twitter_link', 'options');
	}

	public function LinkedinLink()
	{
		return get_field('linkedin_link', 'options');
	}

	public function YoutubeLink()
	{
		return get_field('youtube_link', 'options');
	}

	public function FooterCta()
	{
		return get_field('footer_cta', 'options');
	}

	public function ShowDefaultFootercta ()
	{
		return get_field('show_default_footercta', 'options');
	}

	public function FooterCtaButton ()
	{
		return get_field('footer_cta_button ', 'options');
	}

	public function FooterAwardsHeading()
	{
		return get_field('footer_awards_heading', 'options');
	}

	public function FooterImages()
	{
		return get_field('footer_images', 'options');
	}

	public function LoginLink()
	{
		return get_field('login_link', 'options');
	}

	public function PageHeaderColour()
    {
        return get_field('page_header_colour');
    }

	public function PageHeaderImage()
	{
		$image = get_field('page_header_image');
		return App::getImageUrlFromField($image);
	}
	
	public function HomePageContent()
	{
		return get_field('home_page_content');
	}
	
	public function HomePageDesktopImage()
	{
		$home_page_desktop_image = get_field('home_page_desktop_image');
		return $home_page_desktop_image;
	}

	public function HomePageMobileImage()
	{
		$home_page_mobile_image = get_field('home_page_mobile_image');
		return $home_page_mobile_image;
	}
	
	public function ShowDefaultCta()
	{
		return get_field('show_default_cta');
	}
	
	public function HomePageButton()
	{
		return get_field('home_page_button');
	}

	public function NewsletterBackgroundImage()
	{
		$newsletter_background_image = get_field('newsletter_background_image', 'options');
		return $newsletter_background_image;
	}

	/**
	 * Returns the image alt tag from an image ID or image field in ACF
	 *
	 * @param $image
	 * @return mixed|string
	 */
	public static function getImageAlt($image)
	{
		$alt = '';
		if( null == $image )
		{
			//Empty
			return '';
		}

		if( is_array($image) )
		{
			if( array_key_exists('alt', $image) )
			{
				$alt = $image['alt'];
			}
		}
		else if( is_integer($image) )
		{
			$alt = get_post_meta( $image, '_wp_attachment_image_alt', true );
		}

		return $alt;
	}

	/**
	 * Generates an img tag with all attributes required
	 * @param $image
	 * @param string $size
	 * @param string $classes
	 * @return string
	 */
	public static function generateImgTag($image, $size = 'full', $classes = '')
	{
		$img_html = '';
		if( !empty($image) )
		{
			$image = $image['ID'];
			$title = App::getImageTitle($image);
			$alt = App::getImageAlt($image);
			$image_url = App::getImageUrlFromField($image, $size);
			$img_html = '<img class="' . $classes . '" title="'. $title . '" alt="' . $alt . '" src="' . $image_url . '" />';
		}
		return $img_html;
	}

	/**
	 * Returns a url from a field or attachment ID
	 *
	 * @param $image ID or image field
	 * @param string $size size of the image
	 * @return mixed|string|void image url or null if can't be found
	 */
	public static function getImageUrlFromField($image, $size = 'full')
	{
		$image_url = '';
		if( null == $image || empty($image) )
		{
			//Doesn't exist
			return;
		}

		if( is_array($image) && !empty($image) )
		{
			if( 'full' == $size )
			{
				$image_url = $image['url'];
			}
			else
			{
				$image_url = $image['sizes'][$size];
			}
		}
		else if( is_integer($image) )
		{
			$obj = wp_get_attachment_image_src($image, $size);
			if( !is_wp_error($obj) )
			{
				$image_url = $obj[0];
			}
		}

		return $image_url;
	}

	/**
	 * Returns the site name
	 *
	 * @return string|void
	 */
    public function siteName()
    {
        return get_bloginfo('name');
    }

	/**
	 * Returns the title based on teh type of page or archive
	 *
	 * @return string|void
	 */
    public static function title()
    {

		if ($title = get_field('page_title_override')) {
			return $title;
		} else {
			if (is_home()) {
				if ($home = get_option('page_for_posts', true)) {
					return get_the_title($home);
				}
				return __('Latest Posts', 'sage');
			}
			if (is_archive()) {
				return get_the_archive_title();
			}
			if (is_search()) {
				return sprintf(__('Search Results for %s', 'sage'), get_search_query());
			}
			if (is_404()) {
				return __('Not Found', 'sage');
			}
			return get_the_title();
		}
    }

	/**
	 * Outputs the HTML for GA
	 *
	 * @return string|string[]
	 */
    public static function googleAnalytics()
    {
        //Get the UA Code
        $ua_code = get_field('ga_code', 'options');
        $html = '';
        if( !empty($ua_code) )
        {
            $html = "<!-- Global site tag (gtag.js) - Google Analytics -->
                    <script async src=\"https://www.googletagmanager.com/gtag/js?id={{CODE}}\"></script>
                    <script>
                      window.dataLayer = window.dataLayer || [];
                      function gtag(){dataLayer.push(arguments);}
                      gtag('js', new Date());

                      gtag('config', '{{CODE}}');
                    </script>
                    ";
            $html = str_replace('{{CODE}}', $ua_code, $html);
        }
        return $html;
    }

	public function ctaPopup()
	{
		$popup = get_field('cta_popup', 'options');

		if( array_key_exists('ab_form_id', $popup) )
		{
			// A/B Testing for form, check if a second form is enabled
			$ab_form_id = $popup['ab_form_id'];
			if( $ab_form_id )
			{
				// If a second form is enabled then pick between them
				$random = rand(1, 2);
				if( 2 == $random )
				{
					$popup['form_id'] = $ab_form_id;
				}
			}
		}

		return $popup;
	}

	public function ShowQuizPopup()
	{
		return get_field('show_quiz_popup', 'options');
	}

	public function ShowQuizSidebar()
	{
		return get_field('show_quiz_sidebar', 'options');
	}

	public function QuizSelection()
	{
		$form_id = get_field('quiz_selection', 'options');
		$ab_form_id = get_field('ab_quiz_selection', 'options');
		if( $ab_form_id )
		{
			// A second form exists, pick one at random
			$random = rand(1, 2);
			if( 2 == $random )
			{
				$form_id = $ab_form_id;
			}
		}
		return $form_id;
	}

	public function SidebarButton()
	{
		return get_field('sidebar_button', 'options');
	}

	public function PopupHeading()
	{
		return get_field('popup_heading', 'options');
	}

	public function PopupSubheading()
	{
		return get_field('popup_subheading', 'options');
	}

	public function PopupCopy()
	{
		return get_field('popup_copy', 'options');
	}

	public function QuizDisclaimer()
	{
		return get_field('quiz_disclaimer', 'options');
	}

	public function QuizDisclaimerLink()
	{
		return get_field('quiz_disclaimer_link', 'options');
	}

	public function SidebarEntireSite()
	{
		return get_field('sidebar_entire_site', 'options');
	}

	public function PopupEntireSite()
	{
		if ('1' == get_field('popup_entire_site', 'options'))
		{
			$show_popup = 'true';
		}
		if ('0' == get_field('popup_entire_site', 'options'))
		{
			$show_popup = 'false';
		}
		return $show_popup;
	}
}
