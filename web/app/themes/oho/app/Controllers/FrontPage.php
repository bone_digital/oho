<?php

namespace App\Controllers;

use App\Bone\PageBuilder;
use Sober\Controller\Controller;
use App\Oho\Posts;

class FrontPage extends Controller
{
	public function Layouts()
	{
		wp_reset_query();
		//Using Sections
		$page_builder = new PageBuilder(get_field('sections'), true );

		//Using Modules
		//$page_builder = new PageBuilder(get_field('modules'), false );

		return $page_builder->layouts();
	}

	public function LatestPosts()
	{
		return Posts::GetLatestPosts(get_the_ID());
	}
}
