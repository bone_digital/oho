<?php

namespace App\Controllers;

use App\Bone\PageBuilder;
use Sober\Controller\Controller;
use App\Oho\Posts;

class Single extends Controller
{

	public function __before()
    {
        wp_reset_query();
    }

	public function Layouts()
    {
        //Using Modules
        $page_builder = new PageBuilder(get_field('post_builder'), false );
        return $page_builder->layouts();
    }

	public function ReadTime()
	{
		return get_field('read_time');
	}

	public function FeaturedImage()
	{
		$featured_image = get_field('featured_image');
		return $featured_image;
	}

	public function Date()
	{
		return get_the_date('F Y');
	}

	public function Title()
	{
		return get_the_title();
	}

	public function LatestPosts()
    {
        return Posts::GetLatestPosts(get_the_ID());
    }
}
