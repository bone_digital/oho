<?php

namespace App\Controllers;

use App\Bone\PageBuilder;
use Sober\Controller\Controller;

class Page extends Controller
{

	public function __before()
    {
        wp_reset_query();
    }
	
	public function Layouts()
	{
		//Using Sections
		$page_builder = new PageBuilder(get_field('sections'), true );

		//Using Modules
		//$page_builder = new PageBuilder(get_field('modules'), false );

		return $page_builder->layouts();
	}

	public function Title()
	{
		$title = get_field('page_title_override');
		if( empty($title) )
		{
			$title = get_the_title();
		}
		return $title;
	}
}
