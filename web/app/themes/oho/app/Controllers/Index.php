<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use App\Oho\Posts;

class Index extends Controller
{

    /**
	 * Return posts
	 *
	 * @return array
	 */
    public function Posts()
    {
        return Posts::GetPosts();
    }

    public function Paging()
    {

        $args = array(
            'next_text' => 'Next',
            'prev_text' => 'Previous'
        );

        return paginate_links($args);
    }

}
