<?php

namespace App;

use PHPMailer\PHPMailer\Exception;
use function App\template;

class Ajax
{
	public function __construct()
	{
		add_action( 'wp_ajax_submit_trial_data', [$this, 'submitTrialForm'] );
		add_action( 'wp_ajax_nopriv_submit_trial_data', [$this, 'submitTrialForm'] );
	}

	public function submitTrialForm()
	{
		$res = [
			'success' => false,
			'errors' => 'Something went wrong.',
			'url'	=> null,
		];
		$url = get_field('api_url_base', 'options');
		if( empty($url) )
		{
			//End the call here
			echo json_encode($res);
			die();
		}
		$api_url = $url . 'api/register';

		/*
		$data = array(
			'organization' => [
				'name'	=> 'Testing org 123',
			],
			'user' => [
				'constituent' => [
					'full_name'	=> 'Test f name 123',
					'email'	=> 'someemailaaaddress@something.com',
				],
				'password'	=> 'abc134',
			],
			'meta' => [
				'plan_size'	=> 50,
			]
		);*/
		$data = $_POST['data'];
		$response = wp_remote_post($api_url, [
			'headers'   => array('Content-Type' => 'application/json; charset=utf-8'),
			'body' => json_encode($data),
			'method'	=> 'POST',
		] );

		$body = json_decode($response['body']);
		if( array_key_exists('login_token', $body) )
		{
			$res['success'] = true;
			$res['errors'] = '';
			$res['url'] = $url . '#loginToken=' . $body->login_token;
		}
		else if (array_key_exists('_errors', $body) )
		{
			$errors = '';
			$errors_obj = $body->_errors;
			if(property_exists($errors_obj, 'organization'))
			{
				$error = $errors_obj->organization->name;
				if( !empty($error) )
				{
					if( is_array($error) )
					{
						$error = join(', ', $error);
					}
					$errors .= $error . '<br />';
				}
			}

			if(property_exists($errors_obj, 'user'))
			{
				if(property_exists($errors_obj->user, 'constituent'))
				{
					if(property_exists($errors_obj->user->constituent, 'full_name'))
					{
						$error = $errors_obj->user->constituent->full_name;
						if( !empty($error) )
						{
							if( is_array($error) )
							{
								$error = join(', ', $error);
							}
							$errors .= $error . '<br />';
						}
					}
					if(property_exists($errors_obj->user->constituent, 'email'))
					{
						$error = $errors_obj->user->constituent->email;
						if( !empty($error) )
						{
							if( is_array($error) )
							{
								$error = join(', ', $error);
							}
							$errors .= $error . '<br />';
						}
					}
				}
			}

			if(property_exists($errors_obj, 'user'))
			{
				if(property_exists($errors_obj->user, 'password'))
				{
					$error = $errors_obj->user->pasword;
					if( !empty($error) )
					{
						if( is_array($error) )
						{
							$error = join(', ', $error);
						}
						$errors .= $error . '<br />';
					}
				}
			}

			if(property_exists($errors_obj, 'meta'))
			{
				$error = $errors_obj->meta->plan_size;
				if( !empty($error) )
				{
					if( is_array($error) )
					{
						$error = join(', ', $error);
					}
					$errors .= $error . '<br />';
				}
			}
			$res['errors'] = $errors;
		}

		echo json_encode($res);
		die();
	}

	/**
	 * Exanple Ajax call
	 */
	public function ajaxExample()
	{
		//End ajax call
		die();
	}
}
